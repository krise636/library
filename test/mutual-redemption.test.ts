/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import anyTest, { ExecutionContext, TestInterface } from 'ava';
import { evaluateProgram, createProgram, meepProgram, createFakeBroadcastTransaction, loadDefaultContractData } from './test-util';

// Import Bitcoin Cash related interfaces
import { AuthenticationProgramBCH, AuthenticationErrorCommon } from '@bitauth/libauth';

// Import AnyHedge library
import { AnyHedgeManager } from '../lib/anyhedge';
import { SignedTransactionProposal, UnsignedTransactionProposal } from '../lib/interfaces';
import * as bchUtil from '../lib/util/bitcoincash-util';

// Import fixture data
import { HEDGE_PUBKEY, LONG_PUBKEY, HEDGE_WIF, LONG_WIF, SEVERAL_DIFFERENT_COINS, ORACLE_WIF, CONTRACT_FUNDING_10M_BCH } from './fixture/constants';
import { DUST_LIMIT, SATS_PER_BCH } from '../lib/constants';

const sinon = require('sinon');

// Specify the interface of our test context and define this in the type of `test()`
// See https://github.com/avajs/ava/blob/master/docs/recipes/typescript.md
interface TestContext
{
	program?: AuthenticationProgramBCH;
}
const test = anyTest as TestInterface<TestContext>;

// Stub the broadcast function to return the built transaction rather than broadcasting it
const fakeBroadcast = createFakeBroadcastTransaction();
sinon.stub(bchUtil, 'broadcastTransaction').callsFake(fakeBroadcast);

// Load an AnyHedge manager.
const loadContractManager = function(): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	return manager;
};

const defaultProposal = async function(): Promise<UnsignedTransactionProposal>
{
	// Create a transaction proposal to send 5BCH to hedge and 10BCH to long
	const proposal =
	{
		inputs: SEVERAL_DIFFERENT_COINS,
		outputs:
		[
			{ to: await bchUtil.encodeCashAddressP2PKH(HEDGE_PUBKEY), amount: 5 * SATS_PER_BCH },
			{ to: await bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY), amount: 10 * SATS_PER_BCH },
		],
	};

	return proposal;
};

test('completeMutualRedemption() should redeem when both parties parties sign the same proposal', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal
	const proposal = await defaultProposal();

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualArbitraryPayout(HEDGE_WIF, proposal, contractData.parameters);

	// Simulate: one party encodes and sends it to the other, who decodes it.
	const encodedHedgeProposal = JSON.stringify(hedgeProposal);
	const decodedHedgeProposal = JSON.parse(encodedHedgeProposal);

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualArbitraryPayout(LONG_WIF, proposal, contractData.parameters);

	// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
	const transactionHex = await manager.completeMutualRedemption(decodedHedgeProposal, longProposal, contractData.parameters);

	// For every input, check that the generated unlock script is able to unlock the lock script
	// eslint-disable-next-line max-nested-callbacks
	await Promise.all(proposal.inputs.map(async (input, i) =>
	{
		t.context.program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);
		t.deepEqual(await evaluateProgram(t.context.program), true);
	}));
});

test('completeMutualRedemption() should not redeem when a party includes incorrect redemption data in the proposal', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal
	const proposal = await defaultProposal();

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualArbitraryPayout(HEDGE_WIF, proposal, contractData.parameters);

	// Change the initial proposal so the long signs something different.
	const differentProposal = { ...proposal, locktime: 100 };

	// Create a signed proposal that includes the long's redemption data.
	const differentSignedProposal = await manager.signMutualArbitraryPayout(LONG_WIF, differentProposal, contractData.parameters);

	// Add the long's redemption data to a copy of the initial proposal.
	// This causes both proposals to have the same transaction details, but incorrect signatures.
	const longProposal = { ...proposal, redemptionDataList: differentSignedProposal.redemptionDataList };

	// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction.
	// Note: When live, this should throw an error because the transaction fails,
	//       but since the network behavior is stubbed out, we evaluate the transaction
	//       to assert that the transaction fails.
	const transactionHex = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);

	// For every input, check that the generated unlock script is unable to unlock the lock script.
	// eslint-disable-next-line max-nested-callbacks
	await Promise.all(proposal.inputs.map(async (input, i) =>
	{
		t.context.program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);
		t.deepEqual(await evaluateProgram(t.context.program), AuthenticationErrorCommon.nonNullSignatureFailure);
	}));
});

test('completeMutualRedemption() should fail early if proposal\'s details don\'t match', async (t: ExecutionContext) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal without redemption data.
	const proposal = await defaultProposal() as SignedTransactionProposal;

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualArbitraryPayout(HEDGE_WIF, proposal, contractData.parameters);

	// Change the initial proposal slightly.
	const differentProposal = { ...proposal, locktime: 100 };

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualArbitraryPayout(LONG_WIF, differentProposal, contractData.parameters);

	// Mutual redemption cannot be completed because both proposals contain different transaction details.
	await t.throwsAsync(
		manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters),
		{ message: 'The passed proposals cannot be merged because they have different transaction details.' },
	);
});

test('completeMutualRedemption() should fail early if neither party signed the proposal', async (t: ExecutionContext) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal without redemption data.
	const proposal = await defaultProposal() as SignedTransactionProposal;

	// Mutual redemption cannot be completed because no redemption data is provided
	await t.throwsAsync(
		manager.completeMutualRedemption(proposal, proposal, contractData.parameters),
		{ message: /Transaction proposal does not include any redemption data/ },
	);
});

test('completeMutualRedemption() should fail early if both proposals are signed by the same party', async (t: ExecutionContext) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal
	const proposal = await defaultProposal();

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualArbitraryPayout(HEDGE_WIF, proposal, contractData.parameters);

	// Mutual redemption cannot be completed because both proposals are signed by the hedge side
	await t.throwsAsync(
		manager.completeMutualRedemption(hedgeProposal, hedgeProposal, contractData.parameters),
		{ message: /Mutual redemption could not successfully be completed/ },
	);
});

test('signMutualArbitraryPayout() should fail if called with a WIF that does not belong to either party', async (t: ExecutionContext) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal.
	const proposal = await defaultProposal();

	// Function should throw because ORACLE_WIF does not belong to either party.
	await t.throwsAsync(
		manager.signMutualArbitraryPayout(ORACLE_WIF, proposal, contractData.parameters),
		{ message: /The passed private key WIF does not belong to either party in the contract/ },
	);
});

test('signMutualArbitraryPayout() should fail if proposal includes outputs below DUST amount', async (t: ExecutionContext) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal.
	const proposal = await defaultProposal();

	proposal.outputs[0].amount = DUST_LIMIT - 1;

	// Function should throw because ORACLE_WIF does not belong to either party.
	await t.throwsAsync(
		manager.signMutualArbitraryPayout(ORACLE_WIF, proposal, contractData.parameters),
		{ message: `One of the outputs in the transaction proposal is below the DUST amount of ${DUST_LIMIT}.` },
	);
});

test('completeMutualRedemption() should redeem when both parties parties sign the same early maturation proposal', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Use a settlement price of $200
	const settlementPrice = 20000;

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualEarlyMaturation(HEDGE_WIF, CONTRACT_FUNDING_10M_BCH, settlementPrice, contractData.parameters);

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualEarlyMaturation(LONG_WIF, CONTRACT_FUNDING_10M_BCH, settlementPrice, contractData.parameters);

	// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
	const transactionHex = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

test('completeMutualRedemption() should fail early when both parties parties sign different early maturation proposals', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Hedge uses a settlement price of $200
	const hedgeSettlementPrice = 20000;

	// Long uses a settlement price of $300
	const longSettlementPrice = 30000;

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualEarlyMaturation(HEDGE_WIF, CONTRACT_FUNDING_10M_BCH, hedgeSettlementPrice, contractData.parameters);

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualEarlyMaturation(LONG_WIF, CONTRACT_FUNDING_10M_BCH, longSettlementPrice, contractData.parameters);

	// Mutual redemption cannot be completed because both proposals contain different transaction details.
	await t.throwsAsync(
		manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters),
		{ message: 'The passed proposals cannot be merged because they have different transaction details.' },
	);
});

test('completeMutualRedemption() should redeem when both parties parties sign the same refund proposal', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Derive hedge and long's addresses
	const hedgeAddress = await bchUtil.encodeCashAddressP2PKH(HEDGE_PUBKEY);
	const longAddress = await bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualRefund(
		HEDGE_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata, hedgeAddress, longAddress,
	);

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualRefund(
		LONG_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata, hedgeAddress, longAddress,
	);

	// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
	const transactionHex = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

test('completeMutualRedemption() should redeem when both parties parties sign the same refund proposal without providing refund addresses', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualRefund(HEDGE_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata);

	// Create a signed proposal that includes the long's redemption data.
	const longProposal = await manager.signMutualRefund(LONG_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata);

	// Complete the mutual redemption by providing both signed proposals and broadcasting the resulting transaction
	const transactionHex = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

test('completeMutualRedemption() should fail early when both parties parties sign different refund proposals', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Derive hedge and long's addresses
	const hedgeAddress = await bchUtil.encodeCashAddressP2PKH(HEDGE_PUBKEY);
	const longAddress = await bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

	// Create a signed proposal that includes the hedge's redemption data.
	const hedgeProposal = await manager.signMutualRefund(
		HEDGE_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata, hedgeAddress, longAddress,
	);

	// Create a signed proposal that includes the long's redemption data, but has the addresses switched around.
	const longProposal = await manager.signMutualRefund(
		LONG_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata, longAddress, hedgeAddress,
	);

	// Mutual redemption cannot be completed because both proposals contain different transaction details.
	await t.throwsAsync(
		manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters),
		{ message: 'The passed proposals cannot be merged because they have different transaction details.' },
	);
});

test('custodialMutualArbitraryPayout() should redeem', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Generate a transaction proposal
	const proposal = await defaultProposal();

	// Create a complete mutual redemption transaction using the custodial function.
	const transactionHex = await manager.custodialMutualArbitraryPayout(HEDGE_WIF, LONG_WIF, proposal, contractData.parameters);

	// For every input, check that the generated unlock script is able to unlock the lock script
	// eslint-disable-next-line max-nested-callbacks
	await Promise.all(proposal.inputs.map(async (input, i) =>
	{
		t.context.program = await createProgram(manager, contractData, transactionHex, input.satoshis, i);
		t.deepEqual(await evaluateProgram(t.context.program), true);
	}));
});

test('custodialMutualEarlyMaturation() should redeem', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Use a settlement price of $200
	const settlementPrice = 20000;

	// Create a complete settlement transaction using the custodial function.
	const transactionHex = await manager.custodialMutualEarlyMaturation(
		HEDGE_WIF, LONG_WIF, CONTRACT_FUNDING_10M_BCH, settlementPrice, contractData.parameters,
	);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

test('custodialMutualRefund() should redeem', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Derive hedge and long's addresses
	const hedgeAddress = await bchUtil.encodeCashAddressP2PKH(HEDGE_PUBKEY);
	const longAddress = await bchUtil.encodeCashAddressP2PKH(LONG_PUBKEY);

	// Create a complete refund transaction using the custodial function.
	const transactionHex = await manager.custodialMutualRefund(
		HEDGE_WIF, LONG_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata, hedgeAddress, longAddress,
	);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

test('custodialMutualRefund() should redeem without passing refund addresses', async (t: ExecutionContext<TestContext>) =>
{
	// Load AnyHedge Manager and load a contract with dummy data (doesn't matter for mutual redemption)
	const manager = loadContractManager();
	const contractData = await loadDefaultContractData(manager);

	// Create a complete refund transaction using the custodial function.
	const transactionHex = await manager.custodialMutualRefund(
		HEDGE_WIF, LONG_WIF, CONTRACT_FUNDING_10M_BCH, contractData.parameters, contractData.metadata,
	);

	// Check that the generated unlock script is able to unlock the lock script
	t.context.program = await createProgram(manager, contractData, transactionHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);
	t.deepEqual(await evaluateProgram(t.context.program), true);
});

// After each hook that automatically outputs the meep command to debug the failed test
test.afterEach.always((t: ExecutionContext<TestContext>) =>
{
	if(t.passed) return;

	if(t.context.program)
	{
		t.log('Meep the mutualRedeem call:');
		t.log(meepProgram(t.context.program));
	}
});
