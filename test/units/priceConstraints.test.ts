// Allow line comments so that the ContractFixture definition is more readable
/* eslint-disable line-comment-position, no-inline-comments */

import test, { ExecutionContext } from 'ava';
import { AnyHedgeManager } from '../../lib/anyhedge';
import {
	DUST_LIMIT,
	MAX_HIGH_LIQUIDATION_PRICE,
	MAX_START_PRICE, MIN_HEDGE_UNITS,
	MIN_LOW_LIQUIDATION_PRICE,
	MIN_START_PRICE, SATS_PER_BCH,
} from '../../lib/constants';

// Create an instance of the contract manager.
const contractManager = new AnyHedgeManager();

// Sane default parameters regarding price for creating a contract
const SOME_BLOCK_HEIGHT = 640000;
const SOME_BLOCK_MODIFIER = 0;
const SOME_PUBKEY_HEX = '000000000000000000000000000000000000000000000000000000000000000000';
const SOME_HEDGE_UNITS = 10000;
const SOME_START_PRICE = 1000;
const SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER = 1.1;
const SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER = 0.9;

// Derived boundary parameters
const highLiquidationMultiplierForMaxPrice = MAX_HIGH_LIQUIDATION_PRICE / SOME_START_PRICE;
const lowLiquidationMultiplierForMinPrice = MIN_LOW_LIQUIDATION_PRICE / SOME_START_PRICE;
const highLiquidationMultiplierForExtremeLong = (MIN_HEDGE_UNITS * SATS_PER_BCH) / (DUST_LIMIT * SOME_START_PRICE);

type ContractFixture = [
	string,               // description
	number,               // hedgeUnits
	number,               // startPrice
	number,               // highLiquidationPriceMultiplier
	number,               // lowLiquidationPriceMultiplier
	('safe' | 'unsafe'),  // which creation function to use
	(RegExp | null),      // expectedErrorRegExp
];

// These cases test price constraints.
// safe creation tests all precision and redeemability conditions.
// unsafe creation tests only hard redeemability conditions.
const priceConstraintFixtures: ContractFixture[] = [
	// start price range
	[ 'should fail over max start price',  SOME_HEDGE_UNITS,  0.1 + MAX_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_START_PRICE}`) ],
	[ 'should succeed at max start price', SOME_HEDGE_UNITS,        MAX_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should succeed at min start price', SOME_HEDGE_UNITS,        MIN_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should fail below min start price', SOME_HEDGE_UNITS, -0.1 + MIN_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MIN_START_PRICE}`) ],

	// liquidation price range
	[ 'should fail over max high liquidation price',            SOME_HEDGE_UNITS, SOME_START_PRICE, 0.1 + highLiquidationMultiplierForMaxPrice,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_HIGH_LIQUIDATION_PRICE}`) ],
	[ 'should succeed at max high liquidation price',           SOME_HEDGE_UNITS, SOME_START_PRICE,       highLiquidationMultiplierForMaxPrice,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should succeed with high liquidation price over start',  SOME_HEDGE_UNITS, SOME_START_PRICE,                                        1.1,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should fail with high liquidation price equal to start', SOME_HEDGE_UNITS, SOME_START_PRICE,                                        1.0,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp('immediate liquidation') ],
	[ 'should fail with low liquidation price equal to start',  SOME_HEDGE_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                                                1.0, 'safe', new RegExp('immediate liquidation') ],
	[ 'should succeed with low liquidation price under start',  SOME_HEDGE_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                                                0.9, 'safe', null ],
	[ 'should succeed at min low liquidation price',            SOME_HEDGE_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                lowLiquidationMultiplierForMinPrice, 'safe', null ],
	[ 'should fail under min low liquidation price',            SOME_HEDGE_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,         -0.1 + lowLiquidationMultiplierForMinPrice, 'safe', new RegExp(`${MIN_LOW_LIQUIDATION_PRICE}`) ],

	// liquidation price limit due to coupling with minimum payout sats in extreme long case
	[ 'should fail at extreme long high liquidation price limit '
		+ 'but fail because the lowest extreme liquidation price limit '
		+ 'is higher than the absolute max liquidation price limit', MIN_HEDGE_UNITS, SOME_START_PRICE, -0.0001 + highLiquidationMultiplierForExtremeLong, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_HIGH_LIQUIDATION_PRICE}`) ],
	[ 'should fail over extreme long high liquidation price limit',  MIN_HEDGE_UNITS, SOME_START_PRICE,     0.1 + highLiquidationMultiplierForExtremeLong, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp('and high liquidation price multiplier') ],

	// liquidation min max
	[ 'should succeed with low liquidation price over zero', SOME_HEDGE_UNITS,  SOME_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, 0.001, 'unsafe', null ],
	[ 'should fail with low liquidation price at zero',      SOME_HEDGE_UNITS,  SOME_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,   0.0, 'unsafe', new RegExp('cannot be <= 0') ],
];

type ParametersForCreate = [
	string,  // oraclePublicKey
	string,  // hedgePublicKey
	string,  // longPublicKey
	number,  // hedgeUnits
	number,  // startPrice
	number,  // startBlockHeight
	number,  // earliestLiquidationModifier
	number,  // maturityModifier
	number,  // highLiquidationPriceMultiplier
	number,  // lowLiquidationPriceMultiplier
];

// Runs test on price constraints for a given fixture
const runPriceConstraintTest = async function(
	description: string,
	hedgeUnits: number,
	startPrice: number,
	highLiquidationPriceMultiplier: number,
	lowLiquidationPriceMultiplier: number,
	whichCreationFunction: ('safe' | 'unsafe'),
	expectedErrorRegExp: (RegExp | null),
): Promise<void>
{
	const parameters: ParametersForCreate =
	[
		SOME_PUBKEY_HEX,  // oraclePublicKey
		SOME_PUBKEY_HEX,  // hedgePublicKey
		SOME_PUBKEY_HEX,  // longPublicKey
		hedgeUnits,
		startPrice,
		SOME_BLOCK_HEIGHT,  // startBlockHeight
		SOME_BLOCK_MODIFIER,  // earliestLiquidationModifier
		SOME_BLOCK_MODIFIER,  // maturityModifier
		highLiquidationPriceMultiplier,  // highLiquidationPriceMultiplier
		lowLiquidationPriceMultiplier,  // lowLiquidationPriceMultiplier
	];

	test(description, async (t: ExecutionContext) =>
	{
		// Choose to use create() or unsafeCreate()
		const creationFunction = (whichCreationFunction === 'safe') ? contractManager.createContract : contractManager.createContractUnsafe;

		// Confirm that the test succeeds or fails as expected
		if(expectedErrorRegExp === null)
		{
			// Confirm that the creation works fine when no error is expected
			await t.notThrowsAsync(
				creationFunction.call(contractManager, ...parameters),
				`parameters: ${parameters}`,
			);
		}
		else
		{
			// Confirm that the error is as expected when creation should fail
			await t.throwsAsync(
				creationFunction.call(contractManager, ...parameters),
				{ message: expectedErrorRegExp },
				`parameters ${parameters}`,
			);
		}
	});
};

// Run all test fixtures
priceConstraintFixtures.forEach((fixture) => runPriceConstraintTest(...fixture));
