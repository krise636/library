// Create contract data for specified hedge units, start price and protection
import test, { ExecutionContext } from 'ava';
import { OracleData } from '@generalprotocols/price-oracle';
import { ContractData } from '../../lib/interfaces';
import { AnyHedgeManager } from '../../lib/anyhedge';
import { START_BLOCK, ORACLE_PUBKEY, HEDGE_PUBKEY, LONG_PUBKEY, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, ORACLE_WIF } from '../fixture/constants';
import { binToHex } from '@bitauth/libauth';
import { calculateTotalSats } from '../../lib/util/anyhedge-util';

// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	hedgeUnits: number,
	startPrice: number,
): Promise<ContractData>
{
	// Allow for immediate settlement and disallow maturity for the foreseeable future
	const earliestLiquidationModifier = 0;
	const maturityModifier = 1000000;

	const lowLiquidationPriceMultiplier = 1 - DEFAULT_VOLATILITY_PROTECTION;

	const contractData = await manager.createContract(
		ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		hedgeUnits,
		startPrice,
		START_BLOCK,
		earliestLiquidationModifier,
		maturityModifier,
		DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier,
	);

	return contractData;
};

// This test can be used to check network functionality on mainnet.
// It is currently set up to burn money, but we could update it so that it
// doesn't cost money (because we receive the money back from the contract)
// Usage right now:
// 1. Remove '.skip' from the test.
// 2. Run `npm run build`
// 3. Run `./node_modules/.bin/ava test/e2e/network.test.ts --verbose` (Will fail)
// 4. Send around 40k satoshis the logged address
// 5. Run (3) again (Will succeed) - check logged txid
test.skip('should send transaction to the network', async (t: ExecutionContext) =>
{
	// Set up anyhedge manager
	const manager = new AnyHedgeManager();

	// Create a 50 USD cent contract
	const HEDGE_UNITS = 50;

	// Artificially inflate the start price x10, so that it is less expensive to run this test.
	const START_PRICE = 200000;

	// Load contract data
	const contractData = await loadContractData(manager, HEDGE_UNITS, START_PRICE);

	// Log contract address
	t.log(contractData.address);

	// Set up test oracle data
	const LIQUIDATION_PRICE = 10000;
	const oracleMessage = await OracleData.createPriceMessage(
		LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1,
	);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// Get contract funding to use in liquidation
	const [ contractFunding ] = await manager.getContractFundings(contractData.parameters);

	t.truthy(contractFunding, `Contract needs to be funded with ${calculateTotalSats(contractData.metadata)} satoshis`);

	// Liquidate contract
	const txid = await manager.liquidateContractFunding(
		ORACLE_PUBKEY,
		binToHex(oracleMessage),
		binToHex(oracleSignature),
		contractFunding,
		contractData.metadata,
		contractData.parameters,
	);

	// Log txid
	t.log(`https://blockchair.com/bitcoin-cash/transaction/${txid}`);

	t.pass();
});
