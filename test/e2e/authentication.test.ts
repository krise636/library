import { binToHex, encodeTransaction } from '@bitauth/libauth';
import test, { ExecutionContext } from 'ava';
import { AnyHedgeManager } from '../../lib/anyhedge';
import { MissingAuthenticationTokenError } from '../../lib/errors';
import { PROD_ORACLE_PUBKEY, DEFAULT_HEDGE_UNITS, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, HEDGE_PUBKEY, LONG_PUBKEY, START_BLOCK } from '../fixture/constants';

// This test file requires a settlement-services server running at localhost:31157 with token rate limiting off

// Define service domain, scheme and port to use for a localhost server.
const serviceScheme = 'http';
const serviceDomain = 'localhost';
const servicePort = 31157;

// Set up AnyHedgeManager without a token, and with an invalid token
const managerWithoutToken = new AnyHedgeManager({ serviceScheme, serviceDomain, servicePort });
const managerWithInvalidToken = new AnyHedgeManager({ authenticationToken: 'INVALID_TOKEN', serviceScheme, serviceDomain, servicePort });

test('can request a new authentication token when providing name', async (t: ExecutionContext) =>
{
	// Request a token by providing a name
	const token = await managerWithoutToken.requestAuthenticationToken('dummy name');

	// Check that the returned token is a string
	t.truthy(typeof token === 'string');
});

test('cannot request a new authentication token without providing name', async (t: ExecutionContext) =>
{
	// Request a token without providing a name
	const asyncRequest = managerWithoutToken.requestAuthenticationToken(undefined as unknown as string);

	// Check that the request fails
	await t.throwsAsync(asyncRequest);
});

test.skip('cannot request a new authentication token when provided name is empty', async (t: ExecutionContext) =>
{
	// Request a token with an empty string as name
	const asyncRequest = managerWithoutToken.requestAuthenticationToken('');

	// Check that the request fails
	await t.throwsAsync(asyncRequest);
});

test('cannot register contract with missing or invalid authentication token', async (t: ExecutionContext) =>
{
	// Define valid dummy parameters for the register() function
	const dummyParameters =
	[
		PROD_ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		DEFAULT_HEDGE_UNITS,
		DEFAULT_START_PRICE,
		START_BLOCK,
		0,
		10,
		DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		1 - DEFAULT_VOLATILITY_PROTECTION,
	];

	// Attempt to register a contract using both manager instances
	// @ts-ignore
	const requestWithoutToken = managerWithoutToken.registerContractForSettlement(...dummyParameters);
	// @ts-ignore
	const requestWithInvalidToken = managerWithInvalidToken.registerContractForSettlement(...dummyParameters);

	// Check that both requests fail
	await t.throwsAsync(requestWithoutToken, { instanceOf: MissingAuthenticationTokenError });
	await t.throwsAsync(requestWithInvalidToken, { message: 'Request to /contract failed due to undefined authentication token.' });
});

test('cannot submit funding transaction with missing or invalid authentication token', async (t: ExecutionContext) =>
{
	// Define dummy parameters to the submitFundingTransaction() function
	const dummyTransaction = binToHex(encodeTransaction({ version: 2, inputs: [], outputs: [], locktime: 0 }));
	const dummyAddress = 'bitcoincash:pp5s4jpe5pcjnl0ah2qxe7u5mwy63lz8r5ggm8tstg';

	// Attempt to submit a funding transaction using both managers
	const requestWithoutToken = managerWithoutToken.submitFundingTransaction(dummyAddress, dummyTransaction);
	const requestWithInvalidToken = managerWithInvalidToken.submitFundingTransaction(dummyAddress, dummyTransaction);

	// Check that both requests fail
	await t.throwsAsync(requestWithoutToken, { instanceOf: MissingAuthenticationTokenError });
	await t.throwsAsync(requestWithInvalidToken, { message: 'Request to /funding failed due to undefined authentication token.' });
});

test('cannot get fee address with missing or invalid authentication token', async (t: ExecutionContext) =>
{
	// Attempt to request a new fee address using both managers
	const requestWithoutToken = managerWithoutToken.getFeeAddress();
	const requestWithInvalidToken = managerWithInvalidToken.getFeeAddress();

	// Check that both requests fail
	await t.throwsAsync(requestWithoutToken, { instanceOf: MissingAuthenticationTokenError });
	await t.throwsAsync(requestWithInvalidToken, { message: 'Request to /fee failed due to undefined authentication token.' });
});

test('cannot get contract status with missing or invalid authentication token', async (t: ExecutionContext) =>
{
	// Define dummy parameters to the getContractStatus() function
	const dummyAddress = 'bitcoincash:pp5s4jpe5pcjnl0ah2qxe7u5mwy63lz8r5ggm8tstg';

	// Attempt to request a new fee address using both managers
	const requestWithoutToken = managerWithoutToken.getContractStatus(dummyAddress);
	const requestWithInvalidToken = managerWithInvalidToken.getContractStatus(dummyAddress);

	// Check that both requests fail
	await t.throwsAsync(requestWithoutToken, { instanceOf: MissingAuthenticationTokenError });
	await t.throwsAsync(requestWithInvalidToken, { message: 'Request to /status failed due to undefined authentication token.' });
});
