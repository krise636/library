// Allow line comments so that the ContractFixture definition is more readable
/* eslint-disable line-comment-position, no-inline-comments */

import { START_BLOCK, MATURITY_BLOCK } from './constants';
import { AuthenticationErrorBCH, AuthenticationErrorCommon } from '@bitauth/libauth';

// Test cases in this file have been generated using emergent_reason's Python simulation tool and/or spreadsheet

export type ContractFixture = [
	string, // description
	number, // hedgeUnits
	number, // startPrice
	number, // volatilityProtection
	number, // maxPriceIncrease
	number, // oracleHeight
	number, // oraclePrice
	number, // hedgePayout
	number, // longPayout
	(string | true), // expectedResult
];

// These test cases test liquidations using some basic configurations
export const liquidateFixtures: ContractFixture[] =
[
	[ 'should not liquidate when oracle price is zero',                               100000, 22222, 0.25, 10.0, START_BLOCK + 10,       0,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is negative',                           100000, 22222, 0.25, 10.0, START_BLOCK + 10,     -10,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle height is not a valid block number 1',        100000, 22222, 0.25, 10.0, 499999999,          16667,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle height is not a valid block number 2',        100000, 22222, 0.25, 10.0, 500000000,          16667,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle height is before earliest liquidation block', 100000, 22222, 0.25, 10.0, START_BLOCK - 10,   16667,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle height is after maturity block',              100000, 22222, 0.25, 10.0, MATURITY_BLOCK + 1, 16667,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price equals start price',                    100000, 22222, 0.25, 10.0, START_BLOCK + 10,   22222,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (regular size)',            100000, 22222, 0.25, 10.0, START_BLOCK + 10,   16668,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small size)',                 100, 22222, 0.25, 10.0, START_BLOCK + 10,   16668,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large size',            100000000, 22222, 0.25, 10.0, START_BLOCK + 10,   16668,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large protection)',        100000, 22222, 0.95, 10.0, START_BLOCK + 10,    1112,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small protection)',        100000, 22222, 0.05, 10.0, START_BLOCK + 10,   21112,            1,        1, AuthenticationErrorCommon.failedVerify ],
	[ 'should liquidate when oracle price is below price (regular size)',             100000, 22222, 0.25, 10.0, START_BLOCK + 10,   16667,    599988031,      550, true ],
	[ 'should liquidate when oracle price is below price (small size)',                  100, 22222, 0.25, 10.0, START_BLOCK + 10,   16667,       599990,      547, true ],
	[ 'should liquidate when oracle price is below price (large size)',            100000000, 22222, 0.25, 10.0, START_BLOCK + 10,   16667, 599987978786,      546, true ],
	[ 'should liquidate when oracle price is below price (large protection)',         100000, 22222, 0.95, 10.0, START_BLOCK + 10,    1111,   9000900386,      546, true ],
	[ 'should liquidate when oracle price is below price (small protection)',         100000, 22222, 0.05, 10.0, START_BLOCK + 10,   21111,    473686703,      547, true ],
];

// These test cases test maturation using some basic configurations
export const matureFixtures: ContractFixture[] =
[
	[ 'should not mature when oracle price is zero',                   100000, 22222, 0.25, 10.0, MATURITY_BLOCK,          0,         1,         1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not mature when oracle price is negative',               100000, 22222, 0.25, 10.0, MATURITY_BLOCK,        -10,         1,         1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not mature when oracle height is before maturity block', 100000, 22222, 0.25, 10.0, MATURITY_BLOCK - 10, 22222,         1,         1, AuthenticationErrorCommon.failedVerify ],
	[ 'should not mature when oracle height is after maturity block',  100000, 22222, 0.25, 10.0, MATURITY_BLOCK + 10, 22222,         1,         1, AuthenticationErrorCommon.failedVerify ],
	[ 'should mature when oracle height is at maturity block',         100000, 22222, 0.25, 10.0, MATURITY_BLOCK,      22222, 450004530, 149984047, true ],
];

// The test cases specifically test edge cases regarding different configurations of
// the high and low truncation levels used in the contract truncation math (issue #34)
export const truncLevelMaturationFixtures: ContractFixture[] =
[
	[ 'should mature with high,low truncation 0,0',         10,       200, 0.25, 10.0, MATURITY_BLOCK,       210,         4762418,         1905275, true ],
	[ 'should mature with high,low truncation 1,0',        100,       200, 0.25, 10.0, MATURITY_BLOCK,       210,        47619047,        19048166, true ],
	[ 'should mature with high,low truncation 1,1',       1000,        50, 0.25, 10.0, MATURITY_BLOCK,       210,       476190498,      2155388450, true ],
	[ 'should mature with high,low truncation 2,0',    1000000,    200000, 0.25, 10.0, MATURITY_BLOCK,    210000,       476190510,       190476223, true ],
	[ 'should mature with high,low truncation 2,1',      10000,       200, 0.25, 10.0, MATURITY_BLOCK,       210,      4761904674,      1904762402, true ],
	[ 'should mature with high,low truncation 2,1b',   1000000,     20000, 0.25, 10.0, MATURITY_BLOCK,   2000000,       454545954,      6212121122, true ],
	[ 'should mature with high,low truncation 2,2',    1000000,       100, 0.25, 10.0, MATURITY_BLOCK,       210,    476190474786,    857142854178, true ],
	[ 'should mature with high,low truncation 3,0',   10000000,   2000000, 0.25, 10.0, MATURITY_BLOCK,   2100000,       476199714,       190468011, true ],
	[ 'should mature with high,low truncation 3,1',   10000000,     20000, 0.25, 10.0, MATURITY_BLOCK,     21000,     47619046946,     19047620130, true ],
	[ 'should mature with high,low truncation 3,2',   10000000,       200, 0.25, 10.0, MATURITY_BLOCK,       210,   4761904677410,   1904761963042, true ],
	[ 'should mature with high,low truncation 3,3',  100000000,        50, 0.25, 10.0, MATURITY_BLOCK,       210,  47619040608802, 215538840109602, true ],
	// This contract should NEVER be created because 4-byte delta almost always results
	// in either extension to a 5-byte integer or a truncation to zero.
	[ 'should FAIL   with high,low truncation 4,0', 1000000000, 190000000, 0.25, 10.0, MATURITY_BLOCK, 190000000,               1,               1, AuthenticationErrorBCH.divisionByZero ],
	[ 'should mature with high,low truncation 4,1', 1000000000,  20000000, 0.25, 10.0, MATURITY_BLOCK,  21000000,      4762538786,      1904127778, true ],
	[ 'should mature with high,low truncation 4,2', 1000000000,    200000, 0.25, 10.0, MATURITY_BLOCK,    210000,    476191851042,    190474813986, true ],
	[ 'should mature with high,low truncation 4,3', 1000000000,       200, 0.25, 10.0, MATURITY_BLOCK,       210, 476190456414754, 190476195988002, true ],
	// We do not test 4,4 because the output values are too large for JS (and require more than 21M BCH to exist)
];

// These test cases specifically test edge cases regarding different configurations of
// the mod extension / price truncation used in the contract truncation math (issue #84)
export const modExtensionMaturationFixtures: ContractFixture[] =
[
	[ 'should mature with (mod extension, price truncation (0,0)',         20,      1000, 0.25, 10.0, MATURITY_BLOCK,     10000,     200546, 2466667, true ],
	[ 'should mature with (mod extension, price truncation (0,1) a',     5000, 110000000, 0.25,  5.0, MATURITY_BLOCK,  78125001,       6062,     547, true ],
	[ 'should mature with (mod extension, price truncation (1,0) a',     5000, 100000000, 0.25,  5.0, MATURITY_BLOCK,  78125000,       6946,     811, true ],
	[ 'should mature with (mod extension, price truncation (0,2) b',    20000,  20000000, 0.25, 10.0, MATURITY_BLOCK,  30517579,      66175,   68219, true ],
	[ 'should mature with (mod extension, price truncation (2,0) b',    20000,  20000000, 0.25, 10.0, MATURITY_BLOCK,  30517578,      66082,   68342, true ],
	[ 'should mature with (mod extension, price truncation (1,1) c',    20000,  20000000, 0.25, 10.0, MATURITY_BLOCK,  30484810,      66150,   68274, true ],
	[ 'should mature with (mod extension, price truncation (2,0) c',    20000,  20000000, 0.25, 10.0, MATURITY_BLOCK,  30484811,      66150,   68274, true ],
	[ 'should mature with (mod extension, price truncation (1,2) d',  2000000,  10000000, 0.25, 10.0, MATURITY_BLOCK,  11888160,   16824107, 9843618, true ],
	[ 'should mature with (mod extension, price truncation (2,1) d',  2000000,  10000000, 0.25, 10.0, MATURITY_BLOCK,  11888161,   16823970, 9843243, true ],
	[ 'should mature with (mod extension, price truncation (3,0) d',  2000000,  10000000, 0.25, 10.0, MATURITY_BLOCK,  11920928,   16777762, 9889451, true ],
	[ 'should FAIL   with (mod extension, price truncation (0,3) d',  2000000,  10000000, 0.25, 10.0, MATURITY_BLOCK,  11920929,          1,       1, AuthenticationErrorBCH.divisionByZero ],
	[ 'should mature with truncated price, raw result is > payout',  10000000,    200000, 0.25, 10.0, MATURITY_BLOCK,    149999, 6666666530,     546, true ],
];
