export interface MethodFixture
{
	input: any[];
	output: any;
}

export interface ContractFixture
{
	createContract: MethodFixture;
	validateContract: MethodFixture;
}

export interface LiquidationFixture
{
	calculateMaturationOutcome: MethodFixture;
}

export interface MaturationFixture
{
	calculateMaturationOutcome: MethodFixture;
}

export interface UseCase
{
	contract: ContractFixture;
	liquidation: LiquidationFixture;
	maturation: MaturationFixture;
}
