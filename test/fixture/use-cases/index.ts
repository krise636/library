import { UseCase } from './interfaces';
import hedge10week from './hedge10week';

export { UseCase } from './interfaces';
export const useCases: { [index: string]: UseCase } =
{
	// Hedge 10 USD for one week.
	hedge10week,
};
