import { UseCase } from './interfaces';

const useCase: UseCase =
{
	/*
	The data in this use case example represents a 1-week long hedging contract of $10.
	The initial data was derived from a functional example with the following on-chain activity:
	NOTE: The data was updated manually at different times because of changes to the library and contract
	*/

	contract:
	{
		createContract:
		{
			input:
			[
				'03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
				'020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				'028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
				1000,
				23600,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output:
			{
				address: 'bitcoincash:pqqmsx6wpytag05yh54qwhnufg9awvf5vc3fpy7v68',
				parameters:
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgeLockScript: '1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPubk: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPubk: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					lowTruncatedZeroes: '',
					highLowDeltaTruncatedZeroes: '00',
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				metadata:
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					startBlockHeight: 615643,
					maturityModifier: 1009,
					earliestLiquidationModifier: 0,
					highLiquidationPriceMultiplier: 10,
					lowLiquidationPriceMultiplier: 0.75,
					startPrice: 23600,
					hedgeUnits: 1000,
					totalInputSats: 5649718,
					hedgeInputSats: 4237288,
					longInputSats: 1412430,
					dustCost: 1092,
					minerCost: 1281,
					longInputUnits: 333.33348,
				},
				version: 'AnyHedge v0.10',
			},
		},
		validateContract:
		{
			input:
			[
				'bitcoincash:pqqmsx6wpytag05yh54qwhnufg9awvf5vc3fpy7v68',
				'03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
				'020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				'028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
				1000,
				23600,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output: true,
		},
	},
	liquidation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgeLockScript: '1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPubk: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPubk: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					lowTruncatedZeroes: '',
					highLowDeltaTruncatedZeroes: '00',
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				5652310,
				17500,
			],
			output:
			{
				hedgePayoutSats: 5650231,
				longPayoutSats: 547,
				payoutSats: 5650778,
				minerFeeSats: 1532,
			},
		},
	},
	maturation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgeLockScript: '1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPubk: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPubk: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					lowTruncatedZeroes: '',
					highLowDeltaTruncatedZeroes: '00',
					hedgeUnitsXSatsPerBchHighTrunc: 390625000,
					payoutSatsLowTrunc: 5649718,
				},
				5652310,
				23500,
			],
			output:
			{
				hedgePayoutSats: 4255351,
				longPayoutSats: 1394431,
				payoutSats: 5649782,
				minerFeeSats: 2528,
			},
		},
	},
};

export default useCase;
