import { binToHex } from '@bitauth/libauth';
import { BITBOX } from 'bitbox-sdk';
import { SATS_PER_BCH } from '../../lib/constants';
import { contractCoinToFunding } from '../../lib/util/anyhedge-util';

const bitbox: BITBOX = new BITBOX();

const rootSeed = bitbox.Mnemonic.toSeed('AnyHedge Tests');
const hdNode = bitbox.HDNode.fromSeed(rootSeed, 'mainnet');

export const PROD_ORACLE_PUBKEY = '02bca289e12298914e45f1afdcb6e6fe822b9f9c7cd671fb195c476ac465725e6f';

export const ORACLE_KEYPAIR = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 0));
export const ORACLE_PUBKEY = binToHex(bitbox.ECPair.toPublicKey(ORACLE_KEYPAIR));
export const ORACLE_WIF = bitbox.ECPair.toWIF(ORACLE_KEYPAIR);

export const HEDGE_KEYPAIR = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 1));
export const HEDGE_PUBKEY = binToHex(bitbox.ECPair.toPublicKey(HEDGE_KEYPAIR));
export const HEDGE_WIF = bitbox.ECPair.toWIF(HEDGE_KEYPAIR);

export const LONG_KEYPAIR = bitbox.HDNode.toKeyPair(bitbox.HDNode.derive(hdNode, 2));
export const LONG_PUBKEY = binToHex(bitbox.ECPair.toPublicKey(LONG_KEYPAIR));
export const LONG_WIF = bitbox.ECPair.toWIF(LONG_KEYPAIR);

export const START_BLOCK = 640000;
export const MATURITY_BLOCK = 641000;
export const DUMMY_HASH = '0000000000000000000000000000000000000000000000000000000000000001';

export const DEFAULT_HEDGE_UNITS = 100000;
export const DEFAULT_START_PRICE = 22222;
export const DEFAULT_VOLATILITY_PROTECTION = 0.25;
export const DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER = 11;
export const DEFAULT_LIQUIDATION_PRICE = 16667;
export const DEFAULT_LIQUIDATION_HEIGHT = START_BLOCK + 10;
export const DEFAULT_HEDGE_PAYOUT = 599988031;
export const DEFAULT_LONG_PAYOUT = 550;

export const COIN_10M_BCH =
{
	txid: DUMMY_HASH,
	vout: 0,
	satoshis: 10e6 * SATS_PER_BCH,
};

export const CONTRACT_FUNDING_10M_BCH = contractCoinToFunding(COIN_10M_BCH);

export const SEVERAL_DIFFERENT_COINS =
[
	{
		txid: '0000000000000000000000000000000000000000000000000000000000000001',
		vout: 0,
		satoshis: 10 * SATS_PER_BCH,
	},
	{
		txid: '0202020202020202020202020202020202020202020202020202020202020202',
		vout: 2,
		satoshis: 5 * SATS_PER_BCH,
	},
	{
		txid: '1212121212121212121212121212121212121212121212121212121212121212',
		vout: 0,
		satoshis: 7 * SATS_PER_BCH,
	},
];

export const SEVERAL_DIFFERENT_FUNDINGS = SEVERAL_DIFFERENT_COINS.map(contractCoinToFunding);
