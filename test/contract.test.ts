/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import anyTest, { ExecutionContext, TestInterface } from 'ava';
import { createFakeBroadcastWithFakeLocktime, evaluateProgram, createProgram, meepProgram } from './test-util';

// Import Bitcoin Cash related interfaces
import { AuthenticationProgramBCH, AuthenticationErrorCommon, binToHex } from '@bitauth/libauth';

// Import AnyHedge library
import { AnyHedgeManager } from '../lib/anyhedge';
import { ContractData, ContractSettlement } from '../lib/interfaces';

// Import fixture data
import { ORACLE_PUBKEY, HEDGE_PUBKEY, LONG_PUBKEY, START_BLOCK, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DUMMY_HASH, ORACLE_WIF, CONTRACT_FUNDING_10M_BCH, MATURITY_BLOCK, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_HEIGHT, DEFAULT_LIQUIDATION_PRICE, DEFAULT_HEDGE_PAYOUT, DEFAULT_LONG_PAYOUT } from './fixture/constants';
import { liquidateFixtures, matureFixtures, truncLevelMaturationFixtures, modExtensionMaturationFixtures } from './fixture/contract.fixture';

// Import price oracle library.
import { OracleData } from '@generalprotocols/price-oracle';

const sinon = require('sinon');

// Specify the interface of our test context and define this in the type of `test()`
// See https://github.com/avajs/ava/blob/master/docs/recipes/typescript.md
interface TestContext
{
	payoutProgram?: AuthenticationProgramBCH;
	matureOrLiquidateProgram?: AuthenticationProgramBCH;
}
const test = anyTest as TestInterface<TestContext>;

// Load contract manager and swaps out the broadcast function
const loadContractManager = function(fakeLocktime?: number): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	// Stub the broadcast function to return the built transaction rather than broadcasting it
	// Also sets a provided fake locktime
	const fakeBroadcast = createFakeBroadcastWithFakeLocktime(fakeLocktime);
	sinon.stub(manager, 'broadcastTransaction').callsFake(fakeBroadcast);

	return manager;
};

// Create contract data for specified hedge units, start price and protection
// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	hedgeUnits: number,
	startPrice: number,
	volatilityProtection: number,
	maxPriceIncrease: number,
): Promise<ContractData>
{
	const earliestLiquidationModifier = 0;
	const maturityModifier = MATURITY_BLOCK - START_BLOCK;
	const lowLiquidationPriceMultiplier = 1 - volatilityProtection;
	const highLiquidationPriceMultiplier = 1 + maxPriceIncrease;

	// During testing we use the createUnsafe() method to bypass safety checks
	const contractData = await manager.createContractUnsafe(
		ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		hedgeUnits,
		startPrice,
		START_BLOCK,
		earliestLiquidationModifier,
		maturityModifier,
		highLiquidationPriceMultiplier,
		lowLiquidationPriceMultiplier,
	);

	return contractData;
};

// Define an interface for the return type of runLiquidate()/runMature()
interface ProgramAndSettlement
{
	program: AuthenticationProgramBCH;
	settlement: ContractSettlement;
}

// Run the liquidate() function for a specified contract with specified oracle message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runLiquidate = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	oracleMessage: Uint8Array,
	oracleSignature: Uint8Array,
): Promise<ProgramAndSettlement>
{
	const settlement = await manager.liquidateContractFunding(
		ORACLE_PUBKEY,
		binToHex(oracleMessage),
		binToHex(oracleSignature),
		CONTRACT_FUNDING_10M_BCH,
		contractData.metadata,
		contractData.parameters,
	);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.spendingTransaction, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the mature() function for a specified contract with specified oracle message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runMature = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	oracleMessage: Uint8Array,
	oracleSignature: Uint8Array,
): Promise<ProgramAndSettlement>
{
	const settlement = await manager.matureContractFunding(
		ORACLE_PUBKEY,
		binToHex(oracleMessage),
		binToHex(oracleSignature),
		CONTRACT_FUNDING_10M_BCH,
		contractData.metadata,
		contractData.parameters,
	);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.spendingTransaction, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the payout() function for a specified contract with specified oracle message and
// payout amounts, and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runPayout = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	oracleMessage: Uint8Array,
	oracleSignature: Uint8Array,
	hedgePayout: number,
	longPayout: number,
): Promise<AuthenticationProgramBCH>
{
	const payoutTxHex = await manager.automatedPayout(
		ORACLE_PUBKEY,
		binToHex(oracleMessage),
		binToHex(oracleSignature),
		hedgePayout,
		longPayout,
		CONTRACT_FUNDING_10M_BCH,
		contractData.parameters,
	);

	// Create Authentication Program that can be evaluated with libauth
	const program = createProgram(manager, contractData, payoutTxHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return program;
};

// Specify the type of the runLiquidate() or runMature() functions above, so
// it can be passed into the runFixtureTest() function
type RunMatureOrLiquidateFunction = (
	manager: AnyHedgeManager,
	contractData: ContractData,
	oracleMessage: Uint8Array,
	oracleSignature: Uint8Array,
) => Promise<ProgramAndSettlement>;

// Runs liquidation/maturation test for a specific fixture
const runFixtureTest = async function(
	runMatureOrLiquidate: RunMatureOrLiquidateFunction,
	description: string,
	hedgeUnits: number,
	startPrice: number,
	volatilityProtection: number,
	maxPriceIncrease: number,
	oracleHeight: number,
	oraclePrice: number,
	hedgePayout: number,
	longPayout: number,
	expectedResult: (string | true),
): Promise<void>
{
	test(description, async (t: ExecutionContext<TestContext>) =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, hedgeUnits, startPrice, volatilityProtection, maxPriceIncrease);

		// Set up test oracle data for passed price and height
		const oracleMessage = await OracleData.createPriceMessage(oraclePrice, oracleHeight, DUMMY_HASH, 1, 1, 1);
		const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

		// Check expected result using payout function (does not perform checks so will not throw preemptively)
		t.context.payoutProgram = await runPayout(manager, contractData, oracleMessage, oracleSignature, hedgePayout, longPayout);
		t.deepEqual(await evaluateProgram(t.context.payoutProgram), expectedResult);

		if(expectedResult === true)
		{
			// Check that liquidate()/mature() function call and contract evaluation succeed
			const { program, settlement } = await runMatureOrLiquidate(manager, contractData, oracleMessage, oracleSignature);
			t.context.matureOrLiquidateProgram = program;
			t.deepEqual(await evaluateProgram(t.context.matureOrLiquidateProgram), expectedResult);

			// Check that the returned settlement contains the expected data
			t.deepEqual(settlement.hedgeSatoshis, hedgePayout);
			t.deepEqual(settlement.longSatoshis, longPayout);
			t.deepEqual(settlement.oracleMessage, binToHex(oracleMessage));
			t.deepEqual(settlement.oracleSignature, binToHex(oracleSignature));
		}
		else
		{
			// Check that liquidate()/mature() function call fails since it performs checks before sending
			// so it should throw preemptively
			await t.throwsAsync(runMatureOrLiquidate(manager, contractData, oracleMessage, oracleSignature));
		}
	});
};

// Test all fixture-based liquidation and maturation tests
liquidateFixtures.forEach((fixture) => runFixtureTest(runLiquidate, ...fixture));
matureFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));
truncLevelMaturationFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));
modExtensionMaturationFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));

// Test additional cases that could not be included in the standardized fixtures

test('should liquidate with default parameters (sanity check)', async (t: ExecutionContext<TestContext>) =>
{
	// Set up test contract
	const manager = loadContractManager();
	const contractData = await loadContractData(
		manager, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	);

	// Set up test oracle data
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// check that liquidate() function does not throw, but the transaction fails
	const { program } = await runLiquidate(manager, contractData, oracleMessage, oracleSignature);
	t.context.matureOrLiquidateProgram = program;
	t.true(await evaluateProgram(t.context.matureOrLiquidateProgram));
});

test('should liquidate when oracle height is the final valid block number', async (t: ExecutionContext<TestContext>) =>
{
	// Set up test contract that sets locktime to the final valid block number
	const locktime = 499999998;
	const manager = loadContractManager(locktime);

	const earliestLiquidationModifier = 0;
	const maturityModifier = 499999999 - START_BLOCK;
	const lowLiquidationPriceMultiplier = 1 - DEFAULT_VOLATILITY_PROTECTION;
	const contractData = await manager.createContractUnsafe(
		ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		DEFAULT_HEDGE_UNITS,
		DEFAULT_START_PRICE,
		START_BLOCK,
		earliestLiquidationModifier,
		maturityModifier,
		DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier,
	);

	// Set up test oracle data
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// check that liquidate() function does not throw and the transaction succeeds
	const { program } = await runLiquidate(manager, contractData, oracleMessage, oracleSignature);
	t.context.matureOrLiquidateProgram = program;
	t.true(await evaluateProgram(t.context.matureOrLiquidateProgram));
});

test('should fail when oracle signature does not match oracle message', async (t: ExecutionContext<TestContext>) =>
{
	// Set up test contract
	const manager = loadContractManager();
	const contractData = await loadContractData(
		manager, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	);

	// Intentionally create an oracle signature over a different message than what is used
	// in the transaction, so the datasig check in the contract should fail
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const differentMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, 20, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(differentMessage, ORACLE_WIF);

	// check that liquidate() function does not throw, but the transaction still fails
	const { program } = await runLiquidate(manager, contractData, oracleMessage, oracleSignature);
	t.context.matureOrLiquidateProgram = program;
	t.deepEqual(await evaluateProgram(t.context.matureOrLiquidateProgram), AuthenticationErrorCommon.nonNullSignatureFailure);
});

test('should fail when oracle height is higher than locktime', async (t: ExecutionContext<TestContext>) =>
{
	// Intentionally sets locktime to a lower value than oracle height
	const manager = loadContractManager(DEFAULT_LIQUIDATION_HEIGHT - 10);
	const contractData = await loadContractData(
		manager, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	);

	// Set up test oracle data
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// check that liquidate() function does not throw, but the transaction still fails
	const { program } = await runLiquidate(manager, contractData, oracleMessage, oracleSignature);
	t.context.matureOrLiquidateProgram = program;
	t.deepEqual(await evaluateProgram(t.context.matureOrLiquidateProgram), AuthenticationErrorCommon.unsatisfiedLocktime);
});

test('should fail when sending incorrect amounts', async (t: ExecutionContext<TestContext>) =>
{
	// Set up test contract
	const manager = loadContractManager();
	const contractData = await loadContractData(
		manager, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	);

	// Set up test oracle data
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, DEFAULT_LIQUIDATION_HEIGHT, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// Intentionally set the hedge payout to an incorrect value
	const incorrectHedgePayout = DEFAULT_HEDGE_PAYOUT - 10;

	// Create Authentication Program that can be evaluated with libauth
	t.context.payoutProgram = await runPayout(manager, contractData, oracleMessage, oracleSignature, incorrectHedgePayout, DEFAULT_LONG_PAYOUT);

	// We check for 'failed verify' rather than 'unsuccessful evaluation' ue to the way CashScript optimizes bytecode
	t.deepEqual(await evaluateProgram(t.context.payoutProgram), AuthenticationErrorCommon.failedVerify);
});

test('should fail when oracle height is negative', async (t: ExecutionContext<TestContext>) =>
{
	// Set up test contract that sets locktime to DEFAULT_LIQUIDATION_HEIGHT
	const manager = loadContractManager(DEFAULT_LIQUIDATION_HEIGHT);
	const contractData = await loadContractData(
		manager, DEFAULT_HEDGE_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	);

	// Set up test oracle data (with a negative oracle height set)
	const oracleMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_PRICE, -10, DUMMY_HASH, 1, 1, 1);
	const oracleSignature = await OracleData.signMessage(oracleMessage, ORACLE_WIF);

	// check that payout() function does not throw, but the transaction still fails
	t.context.payoutProgram = await runPayout(manager, contractData, oracleMessage, oracleSignature, DEFAULT_HEDGE_PAYOUT, DEFAULT_LONG_PAYOUT);
	t.deepEqual(await evaluateProgram(t.context.payoutProgram), AuthenticationErrorCommon.negativeLocktime);
});

test.skip('should fail when multiple inputs are used in one transaction', () =>
{
	// This cannot be tested using the library (because it enforces single inputs), but we should test it in the contracts repo.
});

test.skip('should fail when sending to incorrect addresses', async () =>
{
	// This cannot be tested using the library (because it automatically sends to the correct addresses), but we should test it in the contracts repo.
});

// After each hook that automatically outputs the meep command to debug the failed test
test.afterEach.always((t: ExecutionContext<TestContext>) =>
{
	if(t.passed) return;

	if(t.context.matureOrLiquidateProgram)
	{
		t.log('Meep the mature()/liquidate() call:');
		t.log(meepProgram(t.context.matureOrLiquidateProgram));
	}
	else if(t.context.payoutProgram)
	{
		t.log('Meep the payout() call:');
		t.log(meepProgram(t.context.payoutProgram));
	}
});
