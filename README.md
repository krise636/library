# AnyHedge Library

Library for creation, validation and management of AnyHedge contracts.


## Installation

Install the library via NPM:

```
# npm install @generalprotocols/anyhedge
```


## Introduction

[Full Documentation](https://generalprotocols.gitlab.io/anyhedge/library/index.html)

Developing with AnyHedge is easy but depends on some base knowledge of developing with Bitcoin Cash.
The usage instructions below show the key steps and have links to further documentation.
We also include a [custodial funding example](https://gitlab.com/GeneralProtocols/anyhedge/library/-/blob/development/examples/custodial.js) as a reference that shows both the preparation and the AnyHedge steps.
To run the custodial example, clone the AnyHedge repo and run `npm install` and `npm run build` as prerequisites.


## Usage


### Setting up

Include the AnyHedgeManager into your project.

```js
// Load the AnyHedge library.
const { AnyHedgeManager } = require('@generalprotocols/anyhedge');

// Create an instance of the contract manager with the authentication token included.
const authenticationToken = '<authentication token>';
const manager = new AnyHedgeManager({ authenticationToken });
```

### Requesting an authentication token
In order to use the automated settlement services, you need to request an authentication token.

```js
const authenticationToken = await manager.requestAuthenticationToken('My name');
```

Alternatively you can request an authentication token by sending a direct API request with curl.

```sh
curl -d 'name=My name' "https://api.anyhedge.com:6572/token"
```

This token then needs to be passed into the constructor of an AnyHedgeManager.

### Creating a contract

[Create](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#createContract) a new contract:

```js
// Create new contract.
const contractData = await manager.createContract(...parameters);

// Retrieve its address
const contractAddress = contractData.address;
```

### Registering a contract for automatic redemption

[Register](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#registerContractForSettlement) a contract for automatic settlement:

```js
// Submit a contract for external redemption management.
const contractData = await manager.registerContractForSettlement(...parameters);
```

### Retrieving a registered contract's information

[Get the contract information](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#getContractStatus) for any contract that has been registered with the settlement service:

```js
const contractData = await manager.getContractStatus(contractAddress);
```

### Funding a contract

There are several steps that need to be completed in order to build a valid funding transaction, which are outlined in the example below. After following these steps, [a completed funding transaction can be submitted](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#submitFundingTransaction) to the settlement service so that it can be automatically redeemed.

```js
// STEPS:
// 1. Determine who the parties to the contract will be.
// 2. Determine what unspent coins the parties will use.
// 3. Craft a transaction with the following outputs covered
//     - funding satoshis (hedge input + long input + miner cost (~1500) + dust cost (1092))
//       to the contract's address.
//     - fee satoshis to pay for the automated settlement service
//       (fee rate and address are returned in the initial registration call)
// 4. Pass the transaction to each party for signing.

// Submit the funding transaction to the settlement service
const fundingInformation = await manager.submitFundingTransaction(contractAddress, constructedTransactionHex);
```

### Retrieving a contract's fundings

A contract can be funded in multiple separate instances. [Retrieve a list of the contract's fundings](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#getContractFundings):

```js
const fundings = await manager.getContractFundings(contractData.parameters);
```

### Manually redeeming a contract

If you need to, you can [mature](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#matureContractFunding) or [liquidate](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#liquidateContractFunding) a contract funding manually:

*Note that this is not necessary if you are using a redemption service as above.*

```js
// Mature a contract funding.
const maturationResult = await manager.matureContractFunding(...parameters);

// Liquidate a contract funding.
const liquidationResult = await manager.liquidateContractFunding(...parameters);
```

### Mutual redemption of a contract

Also if needed, you can mutually redeem a contract by agreement between Hedge and Long in several ways:

#### Non-custodial mutual redemption

Usually, both parties of the contract are separate entities that do not wish to share their private keys. So in order to mutually redeem a contract in a non-custodial way, both parties need to sign a transaction separately.

##### Mutual refund

In a [mutual refund](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#signMutualRefund), both parties agree to send the initial input amounts back to both parties.

```js
// Sign a mutual refund proposal on the hedge's side.
const hedgeProposal = await manager.signMutualRefund(...parameters);

// Sign a mutual refund proposal on the long's side.
const longProposal = await manager.signMutualRefund(...parameters);

// Complete the mutual redemption and broadcast the transaction.
const refundTransaction = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);
```

##### Mutual early maturation

In a [mutual early maturation](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#signMutualEarlyMaturation), both parties agree to mimic the behavior of a maturation, but at an arbitrary time.

```js
// Sign a mutual early maturation proposal on the hedge's side.
const hedgeProposal = await manager.signMutualEarlyMaturation(...parameters);

// Sign a mutual early maturation proposal on the long's side.
const longProposal = await manager.signMutualEarlyMaturation(...parameters);

// Complete the mutual redemption and broadcast the transaction.
const earlyMaturationTransaction = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);
```


##### Mutual arbitrary payout

In a [mutual arbitrary payout](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#signMutualArbitraryPayout), both parties agree to any arbitrary transaction proposal, which can include arbitrary inputs, outputs and locktime.

```js
// Sign a mutual early maturation proposal on the hedge's side.
const hedgeProposal = await manager.signMutualArbitraryPayout(...parameters);

// Sign a mutual early maturation proposal on the long's side.
const longProposal = await manager.signMutualArbitraryPayout(...parameters);

// Complete the mutual redemption and broadcast the transaction.
const arbitraryPayoutTransaction = await manager.completeMutualRedemption(hedgeProposal, longProposal, contractData.parameters);
```

#### Custodial mutual redemption

If you have access to the private keys of both parties of the contract, you can also mutually redeem the contract by providing both private keys:

- Perform a [refund](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#custodialMutualRefund) to return the original input amounts to their owners.
- Perform an [early maturation](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#custodialMutualEarlyMaturation) that mimics the behavior of a regular maturation, but an arbitrary time.
- Perform an [arbitrary payout](https://generalprotocols.gitlab.io/anyhedge/library/AnyHedgeManager.html#custodialMutualArbitraryPayout) that can include arbitrary transaction details.

```js
// Refund both parties' initial inputs.
const refundTransaction = await manager.custodialMutualRefund(...parameters);

// Perform an early maturation.
const earlyMaturationTransaction = await manager.custodialMutualEarlyMaturation(...parameters);

// Perform an arbitrary payout transaction.
const arbitraryPayoutTransaction = await manager.custodialMutualArbitraryPayout(...parameters);
```
