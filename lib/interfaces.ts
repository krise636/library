import { OracleData } from '@generalprotocols/price-oracle/dist/typings';
import { NetworkProvider, Recipient, Utxo } from 'cashscript';
import { ElectrumCluster } from 'electrum-cash';

export type ContractVersion = 'AnyHedge v0.10';

export interface AnyHedgeManagerConfig
{
	authenticationToken?: string;
	contractVersion?: ContractVersion;
	serviceDomain?: string;
	servicePort?: number;
	serviceScheme?: 'http' | 'https';
	electrumCluster?: ElectrumCluster;
	networkProvider?: NetworkProvider;
}

export interface ContractParameters
{
	lowLiquidationPrice: number;
	highLiquidationPrice: number;
	earliestLiquidationHeight: number;
	maturityHeight: number;
	oraclePubk: string;
	hedgeLockScript: string;
	longLockScript: string;
	hedgeMutualRedeemPubk: string;
	longMutualRedeemPubk: string;
	lowTruncatedZeroes: string;
	highLowDeltaTruncatedZeroes: string;
	hedgeUnitsXSatsPerBchHighTrunc: number;
	payoutSatsLowTrunc: number;
}

export interface ContractMetadata
{
	oraclePublicKey: string;
	hedgePublicKey: string;
	longPublicKey: string;
	startBlockHeight: number;
	maturityModifier: number;
	earliestLiquidationModifier: number;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
	startPrice: number;
	hedgeUnits: number;
	longInputUnits: number;
	totalInputSats: number;
	hedgeInputSats: number;
	longInputSats: number;
	dustCost: number;
	minerCost: number;
}

export interface ContractHashes
{
	mutualRedemptionDataHash: string;
	payoutDataHash: string;
}

export interface ContractFees
{
	address: string;
	satoshis: number;
}

export interface ContractFunding
{
	fundingTransaction: string;
	fundingOutput: number;
	fundingSatoshis: number;
	feeOutput?: number;
	feeSatoshis?: number;
}

export enum SettlementType
// eslint-disable-next-line @typescript-eslint/indent
{
	MATURATION = 'maturation',
	LIQUIDATION = 'liquidation',
	MUTUAL = 'mutual',
	EXTERNAL = 'external',
}

export interface ContractSettlement
{
	spendingTransaction: string;
	settlementType: SettlementType;
	hedgeSatoshis: number;
	longSatoshis: number;
	oracleMessage?: string;
	oraclePublicKey?: string;
	oracleSignature?: string;
	oraclePrice?: number;
}

export interface ContractData
{
	version: string;
	address: string;
	parameters: ContractParameters;
	metadata: ContractMetadata;
	fee?: ContractFees;
	funding?: ContractFunding[];
	settlement?: ContractSettlement[];
}

// Parsing a transaction gives us no information about the omitted properties.
export type ParsedFunding = Omit<ContractFunding, 'fundingSatoshis' | 'feeOutput' | 'feeSatoshis'>;

// Parsing a transaction can contain any of the fields of a ContractSettlement.
// Technically it should always contain spendingTransaction and settlementType though.
export type ParsedSettlement = Pick<ContractSettlement, 'spendingTransaction' | 'settlementType'> & Partial<ContractSettlement>;

// Parsing a mutual redemption does not give us access to the parameters hidden behind the payoutDataHash.
export type ParsedMutualRedemptionParameters = Omit<ContractParameters, 'oraclePubk' | 'hedgeLockScript' | 'longLockScript'>;

// Parsing a payout does not give us access to the parameters hidden behind the mutualRedemptionDataHash.
export type ParsedPayoutParameters = Omit<ContractParameters, 'hedgeMutualRedeemPubk' | 'longMutualRedeemPubk'>;

// Parsing just a redeem script does not give us access to any of the parameters hidden behind data hashes.
export type ParsedParametersCommon = Omit<ParsedMutualRedemptionParameters, 'hedgeMutualRedeemPubk' | 'longMutualRedeemPubk'>;

export interface ParsedMutualRedemptionData
{
	address: string;
	funding: ParsedFunding;
	settlement: ParsedSettlement;
	parameters: ParsedMutualRedemptionParameters;
}

export interface ParsedPayoutData
{
	address: string;
	funding: ParsedFunding;
	settlement: ParsedSettlement;
	parameters: ParsedPayoutParameters;
}

export type ParsedSettlementData = ParsedMutualRedemptionData | ParsedPayoutData;

// Determine if the type of the parsed settlement data is a mutual redemption.
export const isParsedMutualRedemptionData = function(data: ParsedSettlementData): data is ParsedMutualRedemptionData
{
	return data.settlement.settlementType === SettlementType.MUTUAL;
};

// Determine if the type of the parsed settlement data is a payout.
export const isParsedPayoutData = function(data: ParsedSettlementData): data is ParsedPayoutData
{
	return data.settlement.settlementType === SettlementType.MATURATION || data.settlement.settlementType === SettlementType.LIQUIDATION;
};

export interface SimulationOutput
{
	hedgePayoutSats: number;
	longPayoutSats: number;
	payoutSats: number;
	minerFeeSats: number;
}

export interface UnsignedTransactionProposal
{
	inputs: Utxo[];
	outputs: Recipient[];
	locktime?: number;
}

export type RedemptionDataHex = { [key: string]: string };
export type RedemptionDataBin = { [key: string]: Uint8Array };

export interface SignedTransactionProposal extends UnsignedTransactionProposal
{
	redemptionDataList: RedemptionDataHex[];
}

export type OraclePriceMessage = ReturnType<typeof OracleData.parsePriceMessage>;
