export const SATS_PER_BCH = 100000000;
export const SCRIPT_BIT_MAX = 32;
export const SCRIPT_INT_MAX = (2 ** (SCRIPT_BIT_MAX - 1)) - 1;
export const SCRIPT_INT_MIN = -SCRIPT_INT_MAX;
export const MIN_TRANSACTION_FEE = 1500;
export const MAX_TRUNC_BYTES = 4;
export const DUST_LIMIT = 546;
export const DEFAULT_SERVICE_DOMAIN = 'api.anyhedge.com';
export const DEFAULT_SERVICE_PORT = 6572;
export const DEFAULT_SERVICE_SCHEME = 'https';
export const DEFAULT_CONTRACT_VERSION = 'AnyHedge v0.10';

// These min/max values are due to Karol's boundaries (#93) and the constraints
// we had to place on the contract due to truncation bug (#84)
export const MAX_HEDGE_UNITS = 1000000;
export const MIN_HEDGE_UNITS = 50;
export const MAX_START_PRICE = 2000000;
export const MIN_START_PRICE = 150;
export const MAX_HIGH_LIQUIDATION_PRICE = 2 ** 23 - 1;
export const MIN_LOW_LIQUIDATION_PRICE = 2 ** 7;
