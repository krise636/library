import { OracleData } from '@generalprotocols/price-oracle';
import { Contract, ElectrumNetworkProvider, NetworkProvider, SignatureTemplate, Transaction } from 'cashscript';
import fetch from 'node-fetch';
import { DUST_LIMIT, SATS_PER_BCH, DEFAULT_SERVICE_DOMAIN, DEFAULT_SERVICE_PORT, DEFAULT_SERVICE_SCHEME, SCRIPT_INT_MAX, MAX_HEDGE_UNITS, MIN_HEDGE_UNITS, MAX_START_PRICE, MIN_START_PRICE, MAX_HIGH_LIQUIDATION_PRICE, MIN_LOW_LIQUIDATION_PRICE, DEFAULT_CONTRACT_VERSION } from './constants';
import { ContractHashes, ContractMetadata, ContractParameters, SimulationOutput, ContractData, ContractFunding, ContractVersion, UnsignedTransactionProposal, SignedTransactionProposal, ParsedSettlementData, ContractSettlement, SettlementType, AnyHedgeManagerConfig } from './interfaces';
import { debug, isInt } from './util/javascript-util';
import { untruncScriptNum, calculateRequiredScriptNumTruncation, truncScriptNum, calculateTotalSats, dustsafe, scriptNumSize, deriveRedemptionKeyFromAddress, contractCoinToFunding, contractFundingToCoin, buildPayoutTransaction, estimatePayoutTransactionFee, parseSettlementTransaction, contractFundingToOutpoint } from './util/anyhedge-util';
import { broadcastTransaction, buildLockScriptP2PKH, decodeWIF, derivePublicKey, encodeCashAddressP2PKH, hash160, lockScriptToAddress, sha256 } from './util/bitcoincash-util';
import { binToHex, encodeTransaction, hexToBin, instantiateSecp256k1, utf8ToBin } from '@bitauth/libauth';
import { extractRedemptionDataList, attemptTransactionGeneration, mergeSignedProposals, unlockingDataFromWIF, unlockingDataFromRedemptionData } from './util/mutual-redemption-util';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';
import { MissingAuthenticationTokenError } from './errors';

/**
 * Class that manages AnyHedge contract operations, such as creation, validation, and payout of AnyHedge contracts.
 */
// Disable ESLint prefer-default-export rule because 'export default' does not play well with commonjs
// eslint-disable-next-line import/prefer-default-export
export class AnyHedgeManager
{
	contractVersion: ContractVersion;
	serviceScheme: string;
	serviceDomain: string;
	servicePort: number;
	authenticationToken?: string;
	networkProvider?: NetworkProvider;

	get serviceUrl(): string
	{
		return `${this.serviceScheme}://${this.serviceDomain}:${this.servicePort}`;
	}

	/**
	 * Initializes an AnyHedge Manager using the specified config options. Note that the `networkProvider` and `electrumCluster`
	 * options are mutually exclusive and the `networkProvider` takes precedence. The default network provider automatically
	 * connects and disconnects between network requests, so if you need a persistent connection, please use a custom provider.
	 *
	 * @param [config]                       config object containing configuration options for the AnyHedge Manager.
	 * @param [config.authenticationToken]   authentication token used to authenticate network requests to the settlement service
	 * @param [config.contractVersion]       string denoting which AnyHedge contract version to use.
	 * @param [config.serviceDomain]         fully qualified domain name for the settlement service provider.
	 * @param [config.servicePort]           network port number for the settlement service provider.
	 * @param [config.serviceScheme]         network scheme for the settlement service provider, either 'http' or 'https'.
	 * @param [config.electrumCluster]       electrum cluster to use in BCH network operations.
	 * @param [config.networkProvider]       network provider to use for BCH network operations.
	 *
	 * @see {@link https://gitlab.com/GeneralProtocols/anyhedge/contracts|AnyHedge contracts repository} for a list of contract versions.
	 *
	 * @example const anyHedgeManager = new AnyHedgeManager({ authenticationToken: '<token>' });
	 * @example
	 * const config =
	 * {
	 * 	authenticationToken: '<token>',
	 * 	serviceDomain: 'localhost',
	 * 	servicePort: 6572,
	 * 	serviceScheme: 'http',
	 * 	networkProvider: new ElectrumNetworkProvider('mainnet')
	 * };
	 * const anyHedgeManager = new AnyHedgeManager(config);
	 */
	constructor(config: AnyHedgeManagerConfig = {})
	{
		// Extract service URL components from the config object
		this.serviceScheme = config.serviceScheme || DEFAULT_SERVICE_SCHEME;
		this.serviceDomain = config.serviceDomain || DEFAULT_SERVICE_DOMAIN;
		this.servicePort = config.servicePort || DEFAULT_SERVICE_PORT;

		// Store the contract version
		this.contractVersion = config.contractVersion || DEFAULT_CONTRACT_VERSION;

		// Store the authentication token
		this.authenticationToken = config.authenticationToken;

		if(config.networkProvider)
		{
			// Use the provided network provider for BCH network operations if one is provided.
			this.networkProvider = config.networkProvider;
		}
		else
		{
			// If a custom cluster is provided, connection management is handled by the user.
			const manualConnectionManagement = config.electrumCluster !== undefined;

			// Create a new ElectrumNetworkProvider for BCH network operations, optionally using the provided electrum cluster.
			this.networkProvider = new ElectrumNetworkProvider('mainnet', config.electrumCluster, manualConnectionManagement);
		}
	}

	/*
	// External library functions
	*/

	/**
	 * Request an authentication token from the settlement service. This token
	 * is used to authenticate all requests to the settlement services. Only a single
	 * token needs to be generated per consuming application.
	 *
	 * @param name   name to communicate an identity to the settlement service.
	 *
	 * @throws {Error} if the request failed.
	 * @returns a new authentication token
	 */
	async requestAuthenticationToken(name: string): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'requestAuthenticationToken() <=', arguments ]);

		// Request a new authentication token from the settlement service
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify({ name }),
			headers: { 'Content-Type': 'application/json' },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/token`, requestParameters);

		// Throw the returned error if the request failed
		if(!requestResponse.ok)
		{
			throw(new Error(await requestResponse.text()));
		}

		// Retrieve the authentication from the response if the request succeeded
		const authenticationToken = await requestResponse.text();

		// Output function result for easier collection of test data.
		debug.result([ 'requestAuthenticationToken() =>', authenticationToken ]);

		return authenticationToken;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param contractAddress            contract address to submit funding for.
	 * @param transactionHex             funding transaction as a hex-encoded string.
	 * @param [dependencyTransactions]   list of transaction hex strings of transactions that the funding transaction depends on.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if the API call failed in any way (e.g. the transaction failed to broadcast).
	 * @returns funding information for the registered contract.
	 */
	async submitFundingTransaction(
		contractAddress: string,
		transactionHex: string,
		dependencyTransactions?: string[],
	): Promise<ContractFunding>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'submitFundingTransaction() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Submit the funding transaction to the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify({ contractAddress, transactionHex, dependencyTransactions }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/funding`, requestParameters);

		// If the API call was not successful..
		if(!requestResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await requestResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractFunding = await JSON.parse(await requestResponse.json());

		// Output function result for easier collection of test data.
		debug.result([ 'submitFundingTransaction() =>', contractFunding ]);

		// Return the funding information.
		return contractFunding;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param oraclePublicKey                 compressed public key hex string for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKey                  compressed public key hex string for the hedge party.
	 * @param longPublicKey                   compressed public key hex string for the long party.
	 * @param hedgeUnits                      amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                      starting price (units/BCH) of the contract.
	 * @param startBlockHeight                blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier     minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier  multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier   multiplier for the startPrice determining the lower liquidation price boundary.
	 * @param [feeAddress]                    optional fee address acquired from the settlement service to use for this contract.
	 * @param [allowAddressReuse]             optional flag to indicate if fee addresses should be allowed to be reused across contracts.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @returns contract information for the registered contract.
	 */
	async registerContractForSettlement(
		oraclePublicKey: string,
		hedgePublicKey: string,
		longPublicKey: string,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
		feeAddress?: string,
		allowAddressReuse?: boolean,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'registerContractForSettlement() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		const contractCreationParameters =
		{
			// Public keys
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,

			// Contract parameters
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			feeAddress,
			allowAddressReuse,
		};

		// Register the contract with the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify(contractCreationParameters),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/contract`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractData = await JSON.parse(await contractResponse.json());

		// Write log entry for easier debugging.
		debug.action(`Registered contract '${contractData.address}' with ${this.serviceDomain} for automatic settlement.`);

		// Validate that the contract returned is identical to a contract created locally.
		const contractValidity = await this.validateContract(
			contractData.address,
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// If the contract is invalid..
		if(!contractValidity)
		{
			// Write a log entry explaining the problem and throw the error.
			const errorMsg = `Contract registration for '${contractData.address}' with ${this.serviceDomain} resulted in an invalid contract.`;
			debug.errors(errorMsg);

			throw(new Error(errorMsg));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'registerContractForSettlement() =>', contractData ]);

		// Return the contract information.
		return contractData;
	}

	/**
	 * Request a new fee address from the settlement service.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @returns a new fee address that can be used with the register() call.
	 */
	async getFeeAddress(): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getFeeAddress() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Request a new fee address from the settlement service
		const requestParameters =
		{
			method: 'get',
			headers: { Authorization: this.authenticationToken },
		};
		const feeAddressResponse = await fetch(`${this.serviceUrl}/fee`, requestParameters);

		// If the API call was not successful..
		if(!feeAddressResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await feeAddressResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const { feeAddress } = await JSON.parse(await feeAddressResponse.json());

		// Log the outcome of the request.
		debug.action(`Retrieved new fee address '${feeAddress}' from ${this.serviceDomain}.`);

		// Output function result for easier collection of test data.
		debug.result([ 'getFeeAddress() =>', feeAddress ]);

		// Return the fee new address.
		return feeAddress;
	}

	/**
	 * Request the contract data and status of a contract with the settlement service.
	 *
	 * @param contractAddress   address to retrieve status for
	 * @param [privateKeyWIF]   private key WIF of one of the contract's parties.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if no private key WIF was provided *and* the authentication token is different than the one used for registration.
	 * @throws {Error} if an invalid WIF was provided.
	 * @throws {Error} if a private key WIF was provided that does not belong to either of the contract parties.
	 * @throws {Error} if no contract is registered at the settlement service for the given address.
	 * @throws {Error} if the API call is unsuccessful.
	 * @returns the contract data and status of the contract
	 */
	async getContractStatus(
		contractAddress: string,
		privateKeyWIF?: string,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractStatus() <=', arguments ]);

		// Check that an authentication token is set.
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Initialize empty public key and signature.
		let publicKey = '';
		let signature = '';

		// Generate a public key and signature if a private key WIF was provided.
		if(privateKeyWIF)
		{
			// Decode the private key WIF if it was provided.
			const privateKey = await decodeWIF(privateKeyWIF);

			// Derive the corresponding public key.
			publicKey = await derivePublicKey(privateKey);

			// Hash the contract address to be signed.
			const messageHash = await sha256(utf8ToBin(contractAddress));

			// Sign the message hash using the provided private key.
			const secp256k1 = await instantiateSecp256k1();
			const signatureBin = secp256k1.signMessageHashSchnorr(hexToBin(privateKey), messageHash);

			// Convert the signature to a hex string.
			signature = binToHex(signatureBin);
		}

		// Request the contract's status from the settlement service
		const queryParameters = `contractAddress=${contractAddress}&signature=${signature}&publicKey=${publicKey}`;
		const requestParameters =
		{
			method: 'get',
			headers: { Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/status?${queryParameters}`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractData = await JSON.parse(await contractResponse.json());

		debug.action(`Retrieved contract status for '${contractData.address}' from ${this.serviceDomain}.`);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractStatus() =>', contractData ]);

		return contractData;
	}

	/**
	 * Validates that a given contract address matches specific contract parameters.
	 *
	 * @param contractAddress                  contract address encoded according to the cashaddr specification.
	 * @param oraclePublicKey                  compressed public key hex string for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKey                   compressed public key hex string for the hedge party.
	 * @param longPublicKey                    compressed public key hex string for the long party.
	 * @param hedgeUnits                       amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                       starting price (units/BCH) of the contract.
	 * @param startBlockHeight                 blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier      minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                 exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier   multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier    multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns true if the contract address and parameters match, otherwise false.
	 */
	async validateContract(
		contractAddress: string,
		oraclePublicKey: string,
		hedgePublicKey: string,
		longPublicKey: string,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<boolean>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'validateContract() <=', arguments ]);

		// Prepare the contract.
		const contractData = await this.createContract(
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// Build the contract.
		const contract = await this.compileContract(contractData.parameters);

		// Calculate contract validity.
		const contractValidity = (contractAddress === contract.address);

		if(contractValidity)
		{
			// Write log entry for easier debugging.
			debug.action(`Validated a contract address (${contractAddress}) against provided contract parameters.`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}
		else
		{
			// Write log entry for easier debugging.
			debug.errors(`Failed to validate the provided contract address (${contractAddress}) against provided contract parameters generated address (${contract.address}).`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}

		// Output function result for easier collection of test data.
		debug.result([ 'validateContract() =>', contractValidity ]);

		// Return the validity of the contract.
		return contractValidity;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction with arbitrary transaction details.
	 *
	 * @param hedgePrivateKeyWIF    hedge's private key WIF.
	 * @param longPrivateKeyWIF     long's private key WIF.
	 * @param transactionProposal   unsigned transaction proposal for the mutual redemption.
	 * @param contractParameters    contract parameters of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualArbitraryPayout(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualArbitraryPayout() <=', arguments ]);

		// Sign the proposal using both keys.
		const hedgeProposal = await this.signMutualArbitraryPayout(hedgePrivateKeyWIF, transactionProposal, contractParameters);
		const longProposal = await this.signMutualArbitraryPayout(longPrivateKeyWIF, transactionProposal, contractParameters);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualArbitraryPayout() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that mimics a
	 * maturation before the actual maturation block height.
	 *
	 * @param hedgePrivateKeyWIF   hedge's private key WIF.
	 * @param longPrivateKeyWIF    long's private key WIF.
	 * @param contractFunding      the specific Contract Funding to use in the custodial early maturation.
	 * @param settlementPrice      price to use in settlement.
	 * @param contractParameters   contract parameters of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualEarlyMaturation(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		contractFunding: ContractFunding,
		settlementPrice: number,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualEarlyMaturation() <=', arguments ]);

		// Sign the settlement using both keys.
		const hedgeProposal = await this.signMutualEarlyMaturation(hedgePrivateKeyWIF, contractFunding, settlementPrice, contractParameters);
		const longProposal = await this.signMutualEarlyMaturation(longPrivateKeyWIF, contractFunding, settlementPrice, contractParameters);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualEarlyMaturation() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that refunds
	 * the contract's funds based on the provided contract metadata. Optionally
	 * allows you to provide separate refund addresses. If these are omitted,
	 * the mutual redemption public keys are used to receive the refunds.
	 *
	 * @param hedgePrivateKeyWIF     hedge's private key WIF.
	 * @param longPrivateKeyWIF      long's private key WIF.
	 * @param contractFunding        the specific Contract Funding to use in the custodial refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [hedgeRefundAddress]   hedge's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualRefund(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		contractFunding: ContractFunding,
		contractParameters: ContractParameters,
		contractMetadata: ContractMetadata,
		hedgeRefundAddress?: string,
		longRefundAddress?: string,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualRefund() <=', arguments ]);

		// Sign the refund using hedge key.
		const hedgeProposal = await this.signMutualRefund(
			hedgePrivateKeyWIF, contractFunding, contractParameters, contractMetadata, hedgeRefundAddress, longRefundAddress,
		);

		// Sign the refund using long key.
		const longProposal = await this.signMutualRefund(
			longPrivateKeyWIF, contractFunding, contractParameters, contractMetadata, hedgeRefundAddress, longRefundAddress,
		);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualRefund() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Sign a mutual redemption transaction that mimics a maturation before the
	 * actual maturation block height. Both parties need to call this function with
	 * the same input and settlement price. Both signed transaction proposals must then
	 * be passed into the completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF        private key WIF of one of the contract's parties.
	 * @param contractFunding      the specific Contract Funding to use in the mutual early maturation.
	 * @param settlementPrice      price to use in settlement.
	 * @param contractParameters   contract parameters of the relevant contract.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed settlement transaction proposal.
	 */
	async signMutualEarlyMaturation(
		privateKeyWIF: string,
		contractFunding: ContractFunding,
		settlementPrice: number,
		contractParameters: ContractParameters,
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualEarlyMaturation() <=', arguments ]);

		// Check that the provided settlement price is within the contract's bounds.
		if(settlementPrice < contractParameters.lowLiquidationPrice || settlementPrice > contractParameters.highLiquidationPrice)
		{
			throw(new Error('Settlement price is out of liquidation bounds, which is unsupported by a mutual early maturation.'));
		}

		// Calculate settlement outcome.
		const outcome = await this.calculateSettlementOutcome(contractParameters, contractFunding.fundingSatoshis, settlementPrice);

		// Derive hedge/long settlement addresses.
		const hedgeAddress = lockScriptToAddress(contractParameters.hedgeLockScript.slice(2));
		const longAddress = lockScriptToAddress(contractParameters.longLockScript.slice(2));

		// Build transaction proposal based on these parameters.
		const inputs = [ contractFundingToCoin(contractFunding) ];
		const outputs =
		[
			{ to: hedgeAddress, amount: outcome.hedgePayoutSats },
			{ to: longAddress, amount: outcome.longPayoutSats },
		];
		const unsignedProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualEarlyMaturation() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction that refunds the contract's funds based on
	 * the provided contract metadata. Optionally allows you to provide separate
	 * refund addresses. If these are omitted, the mutual redemption public keys
	 * are used to receive the refunds. Both parties need to call this function with
	 * the same contract funding, contract metadata and refund addresses. Both signed
	 * transaction proposals must then be passed into the completeMutualRedemption()
	 * function to broadcast the transaction.
	 *
	 * @param privateKeyWIF          private key WIF of one of the contract's parties.
	 * @param contractFunding        the specific Contract Funding to use in the mutual refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [hedgeRefundAddress]   hedge's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed refund transaction proposal.
	 */
	async signMutualRefund(
		privateKeyWIF: string,
		contractFunding: ContractFunding,
		contractParameters: ContractParameters,
		contractMetadata: ContractMetadata,
		hedgeRefundAddress?: string,
		longRefundAddress?: string,
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualRefund() <=', arguments ]);

		// If no refund addresses are provided, derive them from the private keys
		const hedgeAddress = hedgeRefundAddress || await encodeCashAddressP2PKH(contractParameters.hedgeMutualRedeemPubk);
		const longAddress = longRefundAddress || await encodeCashAddressP2PKH(contractParameters.longMutualRedeemPubk);

		// To ensure the transaction is valid, we put the minimum output value to DUST
		const hedgeAmount = Math.max(contractMetadata.hedgeInputSats, DUST_LIMIT);
		const longAmount = Math.max(contractMetadata.longInputSats, DUST_LIMIT);

		// Build transaction proposal based on the provided parameters.
		const inputs = [ contractFundingToCoin(contractFunding) ];
		const outputs =
		[
			{ to: hedgeAddress, amount: hedgeAmount },
			{ to: longAddress, amount: longAmount },
		];
		const unsignedProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualRefund() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction proposal with arbitrary transaction details.
	 * Both parties need to call this function with the same transaction details.
	 * Both signed transaction proposals must then be passed into the
	 * completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF         private key WIF of one of the contract's parties.
	 * @param transactionProposal   An unsigned proposal for a transaction.
	 * @param contractParameters    contract parameters for the relevant contract.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @throws {Error} if a valid transaction is generated during the preparation.
	 * @returns updated transaction proposal with the resolved variables added in.
	 */
	async signMutualArbitraryPayout(
		privateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualArbitraryPayout() <=', arguments ]);

		// Check that there are no obvious errors in the transaction proposal.

		// Check that there are no outputs below the DUST amount.
		if(transactionProposal.outputs.find((output) => output.amount < DUST_LIMIT))
		{
			throw(new Error(`One of the outputs in the transaction proposal is below the DUST amount of ${DUST_LIMIT}.`));
		}

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.getRedeemScriptHex();

		// Generate unlocking data from the private key WIF
		const unlockingData = await unlockingDataFromWIF(privateKeyWIF, contractParameters);

		// Create a list of unlocking data for every input (same data for every input).
		const unlockingDataPerInput = transactionProposal.inputs.map(() => unlockingData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(transactionProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the attempt was not successful (should never happen).
		if(attempt.success)
		{
			throw(new Error('Internal Error: should not be able to generate valid mutual redemption without both parties'));
		}

		// Extract the relevant redemption data list from the transaction generation attempt.
		const redemptionDataList = extractRedemptionDataList(attempt);

		// Update the transaction proposal by adding the new redemption data list.
		const signedProposal = { ...transactionProposal, redemptionDataList };

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualArbitraryPayout() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Complete a mutual redemption by generating a valid transaction from both parties'
	 * signed proposals and broadcasting it. Both parties need to generate and sign the same
	 * transaction proposal using signMutualEarlyMaturation(), signMutualRefund() or
	 * signMutualArbitraryPayout().
	 *
	 * @param signedProposal1      transaction proposal signed by one of the two parties.
	 * @param signedProposal2      transaction proposal signed by the other party.
	 * @param contractParameters   contract parameters for the relevant contract.
	 *
	 * @throws {Error} if any proposal is unsigned.
	 * @throws {Error} if the transaction details of both proposals don't match.
	 * @throws {Error} if the redemption data lists of the proposals have different lengths.
	 * @throws {Error} if both proposals are signed by the same party.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async completeMutualRedemption(
		signedProposal1: SignedTransactionProposal,
		signedProposal2: SignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'completeMutualRedemption() <=', arguments ]);

		// Check that the transaction proposal includes redemption data
		if(!signedProposal1.redemptionDataList || !signedProposal2.redemptionDataList)
		{
			const errorMsg = 'Transaction proposal does not include any redemption data. '
				+ 'Make sure that both parties signed their transaction proposals using '
				+ 'signMutualRedemption(), signSettlement() or signRefund().';
			throw(new Error(errorMsg));
		}

		// Merge both proposals, combining their redemption data.
		const mergedProposal = mergeSignedProposals(signedProposal1, signedProposal2);

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.getRedeemScriptHex();

		// Generate unlocking data for all inputs of the transaction using the passed redemption data
		const unlockingDataPerInput = mergedProposal.redemptionDataList.map(unlockingDataFromRedemptionData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(mergedProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the transaction generation didn't fail (happens if the proposal was only signed by a single party)
		if(!attempt.success)
		{
			const errorMsg = 'Mutual redemption could not successfully be completed. '
				+ 'Make sure that the passed proposals are signed by different parties.';
			throw(new Error(errorMsg));
		}

		// Hex encode the generated transaction.
		const transactionHex = binToHex(encodeTransaction(attempt.transaction));

		// Broadcast the transaction.
		const broadcastResult = await broadcastTransaction(transactionHex, this.networkProvider);

		// Output function result for easier collection of test data.
		debug.result([ 'completeMutualRedemption() =>', broadcastResult ]);

		return broadcastResult;
	}

	/**
	 * Liquidates a contract.
	 *
	 * @param oraclePublicKey      compressed public key hex string of the oracle that provided the price message.
	 * @param oracleMessage        price message hex string to use for liquidation.
	 * @param oracleSignature      signature hex string for the price message.
	 * @param hedgePublicKey       compressed public key hex string of the hedge party.
	 * @param longPublicKey        compressed public key hex string of the long party.
	 * @param contractFunding      the specific Contract Funding to use in the payout.
	 * @param contractMetadata     contract metadata required to determine available satoshis.
	 * @param contractParameters   contract parameters required to simulate liquidation outcome.
	 *
	 * @returns ContractSettlement object containing the details of the liquidation.
	 */
	async liquidateContractFunding(
		oraclePublicKey: string,
		oracleMessage: string,
		oracleSignature: string,
		contractFunding: ContractFunding,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
	): Promise<ContractSettlement>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'liquidateContractFunding() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action(`Attempting to liquidate contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the oracle message.
		const oracleData = await OracleData.parsePriceMessage(hexToBin(oracleMessage));

		// Validate that the oracle message block height is not at the maturation height.
		if(oracleData.blockHeight === contractParameters.maturityHeight)
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at its maturation height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the oracle price is strictly outside liquidation boundaries.
		if(oracleData.price > contractParameters.lowLiquidationPrice && oracleData.price < contractParameters.highLiquidationPrice)
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at a price within the contract boundaries.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			contractFunding,
			contractMetadata,
			contractParameters,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'liquidateContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Matures a contract.
	 *
	 * @param oraclePublicKey      compressed public key hex string of the oracle that provided the price message.
	 * @param oracleMessage        price message hex string to use for maturation.
	 * @param oracleSignature      signature hex string for the price message.
	 * @param hedgePublicKey       compressed public key hex string of the hedge party.
	 * @param longPublicKey        compressed public key hex string of the long party.
	 * @param contractFunding      the specific Contract Funding to use in the maturation.
	 * @param contractMetadata     contract metadata required to determine available satoshis.
	 * @param contractParameters   contract parameters required to simulate maturation outcome.
	 *
	 * @returns ContractSettlement object containing the details of the maturation.
	 */
	async matureContractFunding(
		oraclePublicKey: string,
		oracleMessage: string,
		oracleSignature: string,
		contractFunding: ContractFunding,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
	): Promise<ContractSettlement>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'matureContractFunding() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action(`Attempting to mature contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the oracle message.
		const oracleData = await OracleData.parsePriceMessage(hexToBin(oracleMessage));

		// Validate that the oracle messages block height is equal to the contracts maturity height.
		if(oracleData.blockHeight !== contractParameters.maturityHeight)
		{
			// Define an error message
			const errorMessage = `Cannot mature contract funding '${contractFundingToOutpoint(contractFunding)}' before its maturity height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// NOTE: We let the miners validate that the current block height is after the maturity height, to avoid network lookups.

		// Validate the that oracle messages block sequence number is 1.
		if(oracleData.blockSequence !== 1)
		{
			// Define an error message
			const errorMessage = `Cannot mature contract funding '${contractFundingToOutpoint(contractFunding)}' with an oracle block sequence other than 1.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			contractFunding,
			contractMetadata,
			contractParameters,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'matureContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Settles a contract (this includes both maturation and liquidation).
	 *
	 * @param oraclePublicKey      compressed public key hex string of the oracle that provided the price message.
	 * @param oracleMessage        price message hex string to use for settlement.
	 * @param oracleSignature      signature hex string for the price message.
	 * @param hedgePublicKey       compressed public key hex string of the hedge party.
	 * @param longPublicKey        compressed public key hex string of the long party.
	 * @param contractFunding      the specific Contract Funding to use in the settlement.
	 * @param contractMetadata     contract metadata required to determine available satoshis.
	 * @param contractParameters   contract parameters required to simulate settlement outcome.
	 *
	 * @returns ContractSettlement object containing the details of the settlement.
	 */
	async settleContractFunding(
		oraclePublicKey: string,
		oracleMessage: string,
		oracleSignature: string,
		contractFunding: ContractFunding,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
	): Promise<ContractSettlement>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'settleContractFunding() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action(`Attempting to settle contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the oracle message.
		const oracleData = await OracleData.parsePriceMessage(hexToBin(oracleMessage));

		// Validate that the oracle message block height is equal or higher than the contracts earliest liquidate height.
		if(oracleData.blockHeight < contractParameters.earliestLiquidationHeight)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' before its earliest liquidation height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the oracle message block height is not after the maturation height.
		if(oracleData.blockHeight > contractParameters.maturityHeight)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' after its maturation height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the oracles price is not zero or negative.
		if(oracleData.price <= 0)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' at a price of <= 0.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Calculate contract outcomes.
		const totalSats = calculateTotalSats(contractMetadata);
		const outcome = await this.calculateSettlementOutcome(contractParameters, totalSats, oracleData.price);

		// Redeem the contract.
		const settlementTransaction = await this.automatedPayout(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractFunding,
			contractParameters,
		);

		// Mark the settlement as LIQUIDATION by default.
		let settlementType = SettlementType.LIQUIDATION;

		// If the oracle message is the first message of the maturity block, it is a MATURATION.
		if(oracleData.blockHeight === contractParameters.maturityHeight && oracleData.blockSequence === 1)
		{
			settlementType = SettlementType.MATURATION;
		}

		// Assemble a ContractSettlement object representing the settlement.
		const settlementData =
		{
			spendingTransaction: settlementTransaction,
			settlementType: settlementType,
			hedgeSatoshis: outcome.hedgePayoutSats,
			longSatoshis: outcome.longPayoutSats,
			oracleMessage: oracleMessage,
			oraclePublicKey: oraclePublicKey,
			oracleSignature: oracleSignature,
			oraclePrice: oracleData.price,
		};

		// Output function result for easier collection of test data.
		debug.result([ 'settleContractFunding() =>', settlementData ]);

		// Return the settlement result.
		return settlementData;
	}

	/**
	 * Redeems the contract with arbitrary numbers.
	 *
	 * @param oraclePublicKey      compressed public key hex string of the oracle that provided the price message.
	 * @param oracleMessage        price message hex string to use for payout.
	 * @param oracleSignature      signature hex string for the price message.
	 * @param hedgePublicKey       compressed public key hex string of the hedge party.
	 * @param longPublicKey        compressed public key hex string of the long party.
	 * @param hedgePayoutSats      number of satoshis to pay out to the hedge party.
	 * @param longPayoutSats       number of satoshis to pay out to the long party.
	 * @param contractFunding      the specific Contract Funding to use in the payout.
	 * @param contractParameters   contract parameters required to unlock the redemption function.
	 *
	 * @returns the transaction ID of a successful redemption.
	 *
	 * @private
	 */
	async automatedPayout(
		oraclePublicKey: string,
		oracleMessage: string,
		oracleSignature: string,
		hedgePayoutSats: number,
		longPayoutSats: number,
		contractFunding: ContractFunding,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'automatedPayout() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action(`Attempting to payout contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		try
		{
			// Build the contract instance.
			const contract = await this.compileContract(contractParameters);

			// Generate a redeem private key.
			const redeemPrivateKey = await deriveRedemptionKeyFromAddress(contract.address);

			// Store the redeem wallet's public key as the preimage public key.
			const preimagePublicKey = await derivePublicKey(redeemPrivateKey);
			const preimageSigTemplate = new SignatureTemplate(hexToBin(redeemPrivateKey));

			// Build the payout transaction.
			const payoutTransaction = await buildPayoutTransaction(
				contract,
				contractParameters,
				contractFunding,
				preimageSigTemplate,
				preimagePublicKey,
				oracleMessage,
				oracleSignature,
				hedgePayoutSats,
				longPayoutSats,
			);

			// Broadcast the transaction.
			const broadcastResult = await this.broadcastTransaction(payoutTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'automatedPayout() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error)
		{
			// Define a base error message.
			const baseErrorMessage = `Failed to payout contract funding '${contractFundingToOutpoint(contractFunding)}'`;

			// Log an error message.
			debug.errors(`${baseErrorMessage}: `, error);

			// If the error includes a meep command, we remove it before passing it on.
			const errorMessageExcludingMeep = error.message ? error.message.split('\nmeep')[0] : error;

			// Throw the error.
			throw(new Error(`${baseErrorMessage}: ${errorMessageExcludingMeep}`));
		}
	}

	/*
	// Internal library functions
	*/

	/**
	 * Wrapper that broadcasts a prepared transaction using the CashScript SDK.
	 *
	 * @param transactionBuilder   fully prepared transaction builder ready to execute its broadcast() function.
	 *
	 * @returns the transaction ID of a successful transaction.
	 *
	 * @private
	 */
	async broadcastTransaction(transactionBuilder: Transaction): Promise<string>
	{
		try
		{
			// Broadcast the raw transaction
			const { txid } = await transactionBuilder.send();

			return txid;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to broadcast transaction: ', error);

			// Build and log raw transaction hex
			const rawTransactionHex = await transactionBuilder.build();
			debug.errors(rawTransactionHex);

			// Throw the error.
			throw(error);
		}
	}

	/**
	 * Retrieve a list of all ContractFunding instances for a contract.
	 *
	 * @param contractParameters   Contract parameters for the relevant contract.
	 *
	 * @returns list of contract fundings for a contract.
	 */
	async getContractFundings(contractParameters: ContractParameters): Promise<ContractFunding[]>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractFundings() <=', arguments ]);

		// Build the contract.
		const contract = await this.compileContract(contractParameters);

		// Retrieve contract's coins as CashScript UTXOs.
		const coins = await contract.getUtxos();

		// Format the CashScript UTXOs as ContractFunding interfaces.
		const fundings = coins.map(contractCoinToFunding);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractFundings() =>', fundings ]);

		return fundings;
	}

	/**
	 * Simulates contract settlement outcome based on contract parameters, total satoshis in the contract and the redemption price.
	 *
	 * @param contractParameters   contract parameters including price boundaries and truncation information.
	 * @param totalSats            total number of satoshis to simulate distribution of.
	 * @param redeemPrice          price (units/BCH) to base the redemption simulation on.
	 *
	 * @returns the TXID of a successful transaction.
	 *
	 * @private
	 */
	async calculateSettlementOutcome(
		contractParameters: ContractParameters,
		totalSats: number,
		redeemPrice: number,
	): Promise<SimulationOutput>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'calculateSettlementOutcome() <=', arguments ]);

		// Throw an error if provided parameters are not integers
		if(!isInt(totalSats) || !isInt(redeemPrice))
		{
			throw(new Error('Provided parameters must be integers'));
		}

		// Store truncation level information in local variables for brevity
		const highLowDelta = hexToBin(contractParameters.highLowDeltaTruncatedZeroes).byteLength;
		const lowTruncSize = hexToBin(contractParameters.lowTruncatedZeroes).byteLength;

		// calculate the clamped price for the contract outcomes.
		const clampedPrice = Math.max(Math.min(redeemPrice, contractParameters.highLiquidationPrice), contractParameters.lowLiquidationPrice);

		// Divide the untruncated hedge payout sats with the clamped price
		// and untruncate it to the low truncation level.
		const hedgeDivHighTrunc = Math.floor(contractParameters.hedgeUnitsXSatsPerBchHighTrunc / clampedPrice);
		const hedgeDivLowTrunc = untruncScriptNum(hedgeDivHighTrunc, highLowDelta);

		// Mod the untruncated hedge payout sats with the clamped price.
		const hedgeModHighTrunc = contractParameters.hedgeUnitsXSatsPerBchHighTrunc % clampedPrice;

		// Calculate mod extension size and apply mod extension.
		const modExtensionSize = Math.min(4 - scriptNumSize(hedgeModHighTrunc), highLowDelta);
		const hedgeModExt = untruncScriptNum(hedgeModHighTrunc, modExtensionSize);

		// Calculate price truncation size and apply price truncation.
		const priceTruncSize = highLowDelta - modExtensionSize;
		const truncatedPrice = truncScriptNum(clampedPrice, priceTruncSize);

		// Throw an error on division by zero
		if(truncatedPrice === 0)
		{
			throw(new Error('This configuration results in a division by zero'));
		}

		// Calculate the hedge LowTrunc payout from hedge DIV and MOD parts.
		const hedgeSatsLowTrunc = Math.min(hedgeDivLowTrunc + Math.floor(hedgeModExt / truncatedPrice), contractParameters.payoutSatsLowTrunc);
		// Calculate the long LowTrunc payout.
		const longSatsLowTrunc = contractParameters.payoutSatsLowTrunc - hedgeSatsLowTrunc;

		// Untruncate both payout values and add the dust protection.
		const hedgePayoutSats = dustsafe(untruncScriptNum(hedgeSatsLowTrunc, lowTruncSize));
		const longPayoutSats = dustsafe(untruncScriptNum(longSatsLowTrunc, lowTruncSize));

		// Calculate the total payout sats and consider the remainder to be miner fees.
		const payoutSats = hedgePayoutSats + longPayoutSats;
		const minerFeeSats = totalSats - payoutSats;

		const result = { hedgePayoutSats, longPayoutSats, payoutSats, minerFeeSats };

		// Write log entry for easier debugging.
		debug.action('Simulating contract outcomes.');

		// Output function result for easier collection of test data.
		debug.result([ 'calculateSettlementOutcome() =>', result ]);

		// Return the results of the calculation.
		return result;
	}

	/**
	 * Builds a contract instance from contract parameters.
	 *
	 * @param contractParameters   contract parameters required to build the contract instance.
	 *
	 * @returns a contract instance.
	 *
	 * @private
	 */
	async compileContract(contractParameters: ContractParameters): Promise<Contract>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'compileContract() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Creating contract instance.');

		// Compute contract hashes from contract parameters
		const contractHashes = await this.createContractHashes(contractParameters);

		// Retrieve the correct artifact.
		const artifact = AnyHedgeArtifacts[this.contractVersion];

		// Construct correct constructor parameters.
		const parameters =
		[
			contractHashes.mutualRedemptionDataHash,
			contractHashes.payoutDataHash,
			contractParameters.hedgeUnitsXSatsPerBchHighTrunc,
			contractParameters.payoutSatsLowTrunc,
			contractParameters.highLowDeltaTruncatedZeroes,
			contractParameters.lowTruncatedZeroes,
			contractParameters.lowLiquidationPrice,
			contractParameters.highLiquidationPrice,
			contractParameters.earliestLiquidationHeight,
			contractParameters.maturityHeight,
		];

		// Instantiate the contract
		const contract = new Contract(artifact, parameters, this.networkProvider);

		// Output function result for easier collection of test data.
		debug.result([ 'compileContract() =>', `${contract.name} contract with address ${contract.address}` ]);

		// Pass back the contract to the caller.
		return contract;
	}

	/**
	 * Creates a new contract.
	 *
	 * @param oraclePublicKey                  compressed public key hex string for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKey                   compressed public key hex string for the hedge party.
	 * @param longPublicKey                    compressed public key hex string for the long party.
	 * @param hedgeUnits                       amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                       starting price (units/BCH) of the contract.
	 * @param startBlockHeight                 blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier      minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                 exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier   multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier    multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns the contract parameters, metadata and hashes.
	 */
	async createContract(
		oraclePublicKey: string,
		hedgePublicKey: string,
		longPublicKey: string,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createContract() <=', arguments ]);

		// If the hedge units are larger than MAX_HEDGE_UNITS, the contract is
		// at risk of being unredeemable with certain configurations and prices
		// which is why we disallow it in the safe create() function (#84).
		if(hedgeUnits > MAX_HEDGE_UNITS)
		{
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be > ${MAX_HEDGE_UNITS}. `
				+ 'The current value might result in unredeemable contracts.'));
		}

		// If the hedge units are smaller than MIN_HEDGE_UNITS, the contract runs
		// into precision errors according to Karol's math analysis (#93).
		if(hedgeUnits < MIN_HEDGE_UNITS)
		{
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be < ${MIN_HEDGE_UNITS}. `
				+ 'The current value might result in a high level of imprecision in calculations.'));
		}

		// If the start price is too high or too low, the contract runs into precision errors (#93).
		if((startPrice < MIN_START_PRICE) || (MAX_START_PRICE < startPrice))
		{
			throw(new Error(`Start price (${startPrice}) must be in the inclusive range [${MIN_START_PRICE}, ${MAX_START_PRICE}]. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		const contractData = await this.createContractUnsafe(
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			hedgeUnits,
			startPrice,
			startBlockHeight,
			earliestLiquidationModifier,
			maturityModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
		);

		// Check that delta is not larger than 2 (should not happen because of the hedge units check) (#84)
		const truncationSizeDelta = hexToBin(contractData.parameters.highLowDeltaTruncatedZeroes).byteLength;
		const maxDelta = 2;
		if(truncationSizeDelta > maxDelta)
		{
			throw(new Error(`Truncation delta amount (${truncationSizeDelta}) cannot be > ${maxDelta}). `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ 'The current value may cause the contract to be unredeemable. '
				+ `Reducing hedge units (${hedgeUnits}) or reducing start price (${startPrice}) will have the most impact on this value. `
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// Disallow low truncation levels higher than 3, since the only way these can work
		// is if more than 21M BCH exist.
		const truncationSizeLow = hexToBin(contractData.parameters.lowTruncatedZeroes).byteLength;
		const maxTruncationSizeLow = 3;
		if(truncationSizeLow > maxTruncationSizeLow)
		{
			throw(new Error(`Low truncation amount (${truncationSizeLow}) cannot be > ${maxTruncationSizeLow}). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ `Reducing hedge units (${hedgeUnits}) or increasing start price (${startPrice}) will have the most impact on this value. `
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// If the liquidation range is too large on either side, we run into precision errors (#93)
		const { lowLiquidationPrice, highLiquidationPrice } = contractData.parameters;
		if(highLiquidationPrice > MAX_HIGH_LIQUIDATION_PRICE)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be > ${MAX_HIGH_LIQUIDATION_PRICE}. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ `Reducing start price ${startPrice} might fix this.`
				+ `Reducing high liquidation price multiplier ${highLiquidationPriceMultiplier} might fix this.`
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^+3 (mega-JPY) instead of plain JPY.'));
		}
		if(lowLiquidationPrice < MIN_LOW_LIQUIDATION_PRICE)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be < ${MIN_LOW_LIQUIDATION_PRICE}. `
				+ 'The current value might result in a high level of imprecision in calculations. '
				+ `Increasing start price ${startPrice} might fix this.`
				+ `Increasing low liquidation price multiplier ${lowLiquidationPriceMultiplier} might fix this.`
				+ 'An alternative is to use an oracle and related calculations that scale the price, for example JPY x 10^-3 (milli-JPY) instead of plain JPY.'));
		}

		// If the liquidation range is inverted or equal to start price, we get immediate liquidation
		if(highLiquidationPrice <= startPrice)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be <= start price (${startPrice}). `
				+ 'The current value will cause immediate liquidation. '
				+ 'Making high liquidation price multiplier sufficiently > 1 will fix this.'));
		}
		if(lowLiquidationPrice >= startPrice)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be >= start price (${startPrice}). `
				+ 'The current value will cause immediate liquidation. '
				+ 'Making low liquidation price multiplier sufficiently < 1 will fix this.'));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'createContract() =>', contractData ]);

		return contractData;
	}

	/**
	 * Creates a new contract without extra safety checks.
	 *
	 * @param oraclePublicKey                  compressed public key hex string for the oracle that the contract trusts for price messages.
	 * @param hedgePublicKey                   compressed public key hex string for the hedge party.
	 * @param longPublicKey                    compressed public key hex string for the long party.
	 * @param hedgeUnits                       amount in units that the hedge party wants to protect against volatility.
	 * @param startPrice                       starting price (units/BCH) of the contract.
	 * @param startBlockHeight                 blockHeight at which the contract is considered to have been started at.
	 * @param earliestLiquidationModifier      minimum number of blocks from the starting height before the contract can be liquidated.
	 * @param maturityModifier                 exact number of blocks from the starting height that the contract should mature at.
	 * @param highLiquidationPriceMultiplier   multiplier for the startPrice determining the upper liquidation price boundary.
	 * @param lowLiquidationPriceMultiplier    multiplier for the startPrice determining the lower liquidation price boundary.
	 *
	 * @returns the contract parameters, metadata and hashes.
	 *
	 * @private
	 */
	async createContractUnsafe(
		oraclePublicKey: string,
		hedgePublicKey: string,
		longPublicKey: string,
		hedgeUnits: number,
		startPrice: number,
		startBlockHeight: number,
		earliestLiquidationModifier: number,
		maturityModifier: number,
		highLiquidationPriceMultiplier: number,
		lowLiquidationPriceMultiplier: number,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createContractUnsafe() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Preparing a new contract.');

		// There are 4 root numbers from which other values are derived.
		// 1. Low liquidation price.
		// 2. High liquidation price
		// 3. Hedge input satoshis.
		// 4. Composite value representing Hedge's unit value.

		// 1. Low liquidation price: the low price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const lowLiquidationPrice = Math.round(lowLiquidationPriceMultiplier * startPrice);

		// Low liquidation price <= zero may cause the contract to be unredeemable
		if(lowLiquidationPrice <= 0)
		{
			throw(new Error(`Low liquidation price (${lowLiquidationPrice}) cannot be <= 0. `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'Making low liquidation price multiplier sufficiently > 0 will fix this.'));
		}

		// 2. High liquidation price: the high price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		const highLiquidationPrice = Math.round(highLiquidationPriceMultiplier * startPrice);

		// High liquidation price greater than SCRIPT_INT_MAX causes the contract to be unredeemable.
		if(highLiquidationPrice > SCRIPT_INT_MAX)
		{
			throw(new Error(`High liquidation price (${highLiquidationPrice}) cannot be > maximum script integer (${SCRIPT_INT_MAX}). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'Making high liquidation price multiplier sufficiently closer to 1 will fix this.'));
		}

		// High liquidation has a constraint regarding payout in extreme cases where only a small amount of satoshis
		// are needed to payout Hedge. The most extreme case is 1 satoshi. To retain a reasonable step-precision
		// between values, and also to match the reality of dust requirements, we choose the dust limit as the
		// minimum hedge payout. The result of the calculation is that for a requested set of parameters, there is
		// a minimum Hedge units that will satisfy the extreme end of high liquidation conditions.
		// One method of calculation, focused on the input modifier is as follows:
		//   hedgePayoutSats @ highLiquidationPrice >= MIN_HEDGE_PAYOUT_SATS
		//   hedgeUnits * SATS_PER_BCH / highLiquidationPrice >= MIN_HEDGE_PAYOUT_SATS
		//   hedgeUnits >= MIN_HEDGE_PAYOUT_SATS * highLiquidationPrice / SATS_PER_BCH
		//     and for MIN_HEDGE_PAYOUT_SATS = DUST:
		//   hedgeUnits >= DUST * highLiquidationPrice / SATS_PER_BCH
		const dynamicMinHedgeUnits = (DUST_LIMIT * highLiquidationPrice) / SATS_PER_BCH;
		if(hedgeUnits < dynamicMinHedgeUnits)
		{
			// Recalculating around the price multiplier to provide an actionable error message:
			//   highLiquidationPrice <= hedgeUnits * SATS_PER_BCH / DUST
			//     substituting with the high liquidation modifier:
			//   highLiquidationPriceMultiplier * startPrice <= hedgeUnits * SATS_PER_BCH / DUST
			//   highLiquidationPriceMultiplier <= (hedgeUnits * SATS_PER_BCH) / (DUST * startPrice)
			const maxHighLiquidationPriceMultiplier = (hedgeUnits * SATS_PER_BCH) / (DUST_LIMIT * startPrice);
			throw(new Error(`Hedge units (${hedgeUnits}) cannot be < ${dynamicMinHedgeUnits} and high liquidation price multiplier (${highLiquidationPriceMultiplier}) cannot be > ${maxHighLiquidationPriceMultiplier}. `
				+ 'The current values may cause the contract to be unredeemable at high prices.'));
		}

		// 3. Hedge input satoshis.
		// Hedge: Satoshis equal to the hedged unit value at the start price.
		//        The value is rounded to achieve a result as close as possible to intent.
		//        More strict terms such as floor and ceiling are imposed on derivative values.
		// For readability, we also derive the naive values of total and long satoshis which will be adjusted later.
		// Total: Satoshis equal to the hedged unit value at the low liquidation price. I.e. long gets about zero.
		//        The value is ceiling to ensure that the result is *at least* enough to cover hedge value, never less.
		// Long:  Satoshis equal to difference between the total satoshis and hedge satoshis.
		//        The value is recorded for metadata purposes only.
		const hedgeInputSats = Math.round((hedgeUnits * SATS_PER_BCH) / startPrice);
		const naiveTotalInputSats = Math.ceil((hedgeUnits * SATS_PER_BCH) / lowLiquidationPrice);
		const naiveLongInputSats = naiveTotalInputSats - hedgeInputSats;

		// Regarding the truncation operations in the remaining code:
		// Due to current limitations of Bitcoin Cash script, calculations can only be performed with 32-bit numbers.
		// Combined with other current limitations, 32 bits is not enough to handle meaningful amounts of value
		// in AnyHedge. Therefore some mathematical tricks are required to get around the limitation. The trick that
		// AnyHedge currently uses is to truncate bytes from large values and remember how much has been removed
		// so it can be added back later.
		// Specifically, AnyHedge has two levels of truncation required to handle two specific large numbers:
		//   HighTrunc) Required for the contract to do calculations with the composite number calculated below in 4.
		//   LowTrunc)  Required for the contract to do calculations with the payoutSats calculated below.

		// 4. Composite number representing Hedge's unit value.
		// The number is calculated as hedge units * 1e8 sats/bch.
		// This overcomes the current limits of BCH scripting where we have division but no multiplication.
		// The value divided by the price in BCH directly yields satoshis for hedge value at said price.
		// The value is naive because it may require truncation for storage and calculations in the contract.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const naiveHedgeUnitsXSatsPerBch = Math.round(hedgeUnits * SATS_PER_BCH);

		// Calculate the required amount of truncation and record the truncated value.
		const truncationSizeHigh = calculateRequiredScriptNumTruncation(naiveHedgeUnitsXSatsPerBch);
		const hedgeUnitsXSatsPerBchHighTrunc = truncScriptNum(naiveHedgeUnitsXSatsPerBch, truncationSizeHigh);

		// After the 4 root values, we derive the remaining money-related numbers for the contract and metadata.

		// Total sats are truncated as described above if necessary for the final contract value.
		// Total input sats are renamed to payout sats to align with contract parameter names
		const truncationSizeLow = calculateRequiredScriptNumTruncation(naiveTotalInputSats);
		const payoutSatsLowTrunc = truncScriptNum(naiveTotalInputSats, truncationSizeLow);

		// The difference between the high and low truncation is needed in the contract so we calculate it here.
		const truncationSizeDelta = truncationSizeHigh - truncationSizeLow;

		// If the truncation delta is negative, the contract is always unredeemable
		if(truncationSizeDelta < 0)
		{
			throw(new Error(`Truncation delta amount (${truncationSizeDelta}) cannot be < 0). `
				+ 'The current value may cause the contract to be unredeemable. '
				+ 'There may be a fundamental problem with the input parameters.'));
		}

		// Total sats, long input sats, long input units are calculated only for metadata
		const totalInputSats = untruncScriptNum(payoutSatsLowTrunc, truncationSizeLow);
		const longInputSats = totalInputSats - hedgeInputSats;
		const longInputUnits = ((longInputSats / SATS_PER_BCH) * startPrice);

		// In addition to money-related numbers, we derive time-related numbers for the contract and metadata.

		// Earliest liquidation provides a grace period in which the contract cannot settle.
		const earliestLiquidationHeight = startBlockHeight + earliestLiquidationModifier;

		// Maturity provides a deadline after which anyone can settle the contract
		// using the maturity block's first price message.
		const maturityHeight = startBlockHeight + maturityModifier;

		// We also package keys and other fixed values for the contract and metadata.

		// Create hedge and long lock scripts.
		const hedgeLockScript = await buildLockScriptP2PKH(hedgePublicKey);
		const longLockScript = await buildLockScriptP2PKH(longPublicKey);

		// Create hex strings with zeroes matching the low and delta truncation sizes.
		const highLowDeltaTruncatedZeroes = '00'.repeat(truncationSizeDelta);
		const lowTruncatedZeroes = '00'.repeat(truncationSizeLow);

		// Assemble the contract parameters.
		const contractParameters =
		{
			lowLiquidationPrice,
			highLiquidationPrice,
			earliestLiquidationHeight,
			maturityHeight,
			oraclePubk: oraclePublicKey,
			hedgeLockScript,
			longLockScript,
			hedgeMutualRedeemPubk: hedgePublicKey,
			longMutualRedeemPubk: longPublicKey,
			lowTruncatedZeroes,
			highLowDeltaTruncatedZeroes,
			hedgeUnitsXSatsPerBchHighTrunc,
			payoutSatsLowTrunc,
		};

		// Build the corresponding contract
		const contract = await this.compileContract(contractParameters);

		// Store the total dust protection cost.
		const dustCost = 2 * DUST_LIMIT;

		// Estimate the miner cost for the payout transaction size (paying 1.0 sats/b).
		const minerCost = await estimatePayoutTransactionFee(contract, contractParameters, 1.0);

		// Assemble the contract metadata.
		const contractMetadata =
		{
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			startBlockHeight,
			maturityModifier,
			earliestLiquidationModifier,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			startPrice,
			hedgeUnits,
			longInputUnits,
			totalInputSats,
			hedgeInputSats,
			longInputSats,
			dustCost,
			minerCost,
		};

		// Assemble the final contract data.
		const contractData =
		{
			version: this.contractVersion,
			address: contract.address,
			parameters: contractParameters,
			metadata: contractMetadata,
		};

		// Output function result for easier collection of test data.
		debug.result([ 'createContractUnsafe() =>', contractData ]);

		// Pass back the contract data to the caller.
		return contractData;
	}

	/**
	 * Computes contract hashes from contract parameters
	 *
	 * @param contractParameters parameters of the contract
	 *
	 * @returns the hashes corresponding to the passed parameters
	 */
	async createContractHashes(contractParameters: ContractParameters): Promise<ContractHashes>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createContractHashes() <=', arguments ]);

		// Assemble the mutual redemption data.
		const { hedgeMutualRedeemPubk, longMutualRedeemPubk } = contractParameters;
		const mutualRedemptionData = [ hedgeMutualRedeemPubk, longMutualRedeemPubk ];

		// Assemble the payout data.
		const { hedgeLockScript, longLockScript, oraclePubk } = contractParameters;
		const payoutData = [ hedgeLockScript, longLockScript, oraclePubk ];

		// Calculate the hashes required to create a contract instance.
		const mutualRedemptionDataHash = binToHex(await hash160(hexToBin(mutualRedemptionData.join(''))));
		const payoutDataHash = binToHex(await hash160(hexToBin(payoutData.join(''))));

		// Assemble ContractHashes interface.
		const contractHashes = { mutualRedemptionDataHash, payoutDataHash };

		// Output function result for easier collection of test data.
		debug.result([ 'createContractHashes() =>', contractHashes ]);

		return contractHashes;
	}

	/**
	 * Parse a settlement transaction to extract as much data as possible, ending up with partial
	 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
	 * could be retrieved.
	 *
	 * @param settlementTransactionHex   hex string for the settlement transaction
	 *
	 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
	 * @throws {SettlementParseError} if the transaction does not have exactly one input.
	 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
	 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
	 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
	 * @returns partial ContractParameters, ContractSettlement, and ContractFunding objects. See {@link https://gitlab.com/GeneralProtocols/anyhedge/library/-/blob/development/examples/parse-settlement-transaction.js|examples/parse-settlement-transaction.js} to inspect the data that this function returns.
	 */
	async parseSettlementTransaction(settlementTransactionHex: string): Promise<ParsedSettlementData>
	{
		return parseSettlementTransaction(settlementTransactionHex);
	}
}
