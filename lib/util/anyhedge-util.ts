import { debug, range } from './javascript-util';
import { SCRIPT_INT_MAX, MAX_TRUNC_BYTES, DUST_LIMIT } from '../constants';
import { ContractFunding, ContractMetadata, ContractParameters, OraclePriceMessage, ParsedFunding, ParsedParametersCommon, ParsedSettlementData, SettlementType } from '../interfaces';
import Long from 'long';
import { bigIntToScriptNumber, instantiateBIP32Crypto, deriveHdPrivateNodeFromSeed, hexToBin, binToHex, decodeTransactionUnsafe, AuthenticationInstructionPush, parseBytecode, binToBigIntUint64LE, Output, getTransactionHashLE, instantiateSha256 } from '@bitauth/libauth';
import { addressToLockScript, buildLockScriptP2PKH, buildLockScriptP2SH, lockScriptToAddress } from './bitcoincash-util';
import { Contract, SignatureTemplate, Transaction, Utxo } from 'cashscript';
import { OracleData } from '@generalprotocols/price-oracle';
import { SettlementParseError } from '../errors';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';
import { Data } from 'cashc';

/**
* Helper function to bit-shift down with truncation.
*
* @param data    a number to bit-shift and truncate.
* @param bytes   how many bytes to bit-shift.
*
* @returns the bit-shifted and then truncated number.
*/
export const truncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'truncScriptNum() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data / (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'truncScriptNum() =>', result ]);

	// Return truncated number.
	return result;
};

/**
* Helper function to bit-shift up with truncation.
*
* @param data    a number to bit-shift and truncate.
* @param bytes   how many bytes to bit-shift.
*
* @returns the bit-shifted and then truncated number.
*/
export const untruncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'untruncScriptNum() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data * (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'untruncScriptNum() =>', result ]);

	// Return untruncated number.
	return result;
};

/**
* Helper function to determine how many bytes we need to shift to fit a number
* within the bitcoin cash script limitations.
*
* @param value   a number to determine bit-shift requirements for.
*
* @returns an integer stating how many bytes we need to shift the value.
*/
export const calculateRequiredScriptNumTruncation = function(value: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateRequiredScriptNumTruncation() <=', arguments ]);

	let truncationLevel;
	for(const truncationSize in range(MAX_TRUNC_BYTES + 1))
	{
		// Truncate the value at this size.
		const truncatedValue = truncScriptNum(value, Number(truncationSize));

		// Check if this truncation size is sufficient..
		if(truncatedValue >= 1 && truncatedValue <= SCRIPT_INT_MAX)
		{
			// Store the current size as a suitable truncation level.
			truncationLevel = Number(truncationSize);

			// Stop looking now that we have a suitable truncation level.
			break;
		}
	}

	if(truncationLevel === undefined)
	{
		// Since no truncation size was matched, throw an error.
		throw(new Error(`Failed to find a suitable truncation level for '${value}'`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'calculateRequiredScriptNumTruncation() =>', truncationLevel ]);

	// return the matching truncation size.
	return truncationLevel;
};

/**
 * Helper function to calculate the size of a script num
 *
 * @param scriptNum   a number to check the script num size.
 *
 * @returns the byte size of the passed number when encoded as a script num.
 */
export const scriptNumSize = function(scriptNum: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'scriptNumSize() <=', arguments ]);

	// Encode the number as a script number and take the byte length
	const result = bigIntToScriptNumber(BigInt(scriptNum)).byteLength;

	// Output function result for easier collection of test data.
	debug.result([ 'scriptNumSize() =>', result ]);

	return result;
};

/**
* Helper function to calculate the total sats for a contract (inputs, miner fees, dust cost)
*
* @param contractMetadata   a ContractMetadata object including the sats information for a contract.
*
* @returns the number of total sats that are contained in the contract.
*/
export const calculateTotalSats = function(contractMetadata: ContractMetadata): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTotalSats() <=', arguments ]);

	// Calculate total sats
	const { hedgeInputSats, longInputSats, minerCost, dustCost } = contractMetadata;
	const totalSats = hedgeInputSats + longInputSats + minerCost + dustCost;

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTotalSats() =>', totalSats ]);

	return totalSats;
};

/**
* Helper function to make an output amount of sats dust safe by performing a bitwise OR
* with the DUST value (546)
*
* @param satoshis   a number of satoshis to make dust safe.
*
* @returns the dust safe version of the number of satoshis.
*/
export const dustsafe = function(satoshis: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'dustSafe() <=', arguments ]);

	// Calculate total sats
	const result = Long.fromNumber(satoshis)
		.or(DUST_LIMIT)
		.toNumber();

	// Output function result for easier collection of test data.
	debug.result([ 'dustSafe() =>', result ]);

	return result;
};

/**
 * Generates a private key hex string used in the redemption of an AnyHedge contract
 * with the provided address
 *
 * @param address   the contract's address
 *
 * @returns the private key used to redeem the AnyHedge contract
 */
export const deriveRedemptionKeyFromAddress = async function(address: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'deriveRedemptionKeyFromAddress() <=', arguments ]);

	// Initialize crypto
	const cryptoBIP32 = await instantiateBIP32Crypto();

	// Use the contract's lock script as the wallet seed
	const seed = hexToBin(addressToLockScript(address));

	// Derive an HD node using the seed and extract its private key
	const hdNode = deriveHdPrivateNodeFromSeed(cryptoBIP32, seed, true);
	const privateKey = binToHex(hdNode.privateKey);

	// Output function result for easier collection of test data.
	debug.result([ 'deriveRedemptionKeyFromAddress() =>', privateKey ]);

	return privateKey;
};

/**
 * Convert a CashScript UTXO to a ContractFunding interface.
 *
 * @param coin   CashScript UTXO to be converted.
 *
 * @returns ContractFunding interface.
 */
export const contractCoinToFunding = function(coin: Utxo): ContractFunding
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractCoinToFunding() <=', arguments ]);

	// Format the CashScript UTXO to match the ContractFunding interface
	const funding =
	{
		fundingTransaction: coin.txid,
		fundingOutput: coin.vout,
		fundingSatoshis: coin.satoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractCoinToFunding() =>', funding ]);

	return funding;
};

/**
 * Convert a ContractFunding interface to a CashScript UTXO.
 *
 * @param funding   contract funding interface.
 *
 * @returns CashScript UTXO.
 */
export const contractFundingToCoin = function(funding: ContractFunding): Utxo
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractFundingToCoin() <=', arguments ]);

	// Format the ContractFunding to match the CashScript UTXO interface
	const coin =
	{
		txid: funding.fundingTransaction,
		vout: funding.fundingOutput,
		satoshis: funding.fundingSatoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractFundingToCoin() =>', coin ]);

	return coin;
};

/**
 * @param funding   contract funding.
 *
 * @returns contract funding UTXO's outpoint as a hex string.
 */
export const contractFundingToOutpoint = function(funding: ContractFunding): string
{
	return `${funding.fundingTransaction}:${funding.fundingOutput}`;
};

/**
 * Build a CashScript Transaction instance for a payout transaction.
 *
 * @param contract             CashScript contract to use for building the transaction.
 * @param contractParameters   contract parameters to use in the payout transaction.
 * @param contractFunding      contract funding to spend from in the payout transaction.
 * @param redeemSignature      signature (template) to use in the payout transaction.
 * @param redeemPublicKey      public key to use in the payout transaction.
 * @param oracleMessage        oracle price message used in the payout transaction.
 * @param oracleSignature      signature over the oracle message.
 * @param hedgePayoutSats      amount to be paid out to the hedge.
 * @param longPayoutSats       amount to be paid out to the long.
 *
 * @returns a CashScript Transaction for a payout transaction with the correct details.
 */
export const buildPayoutTransaction = async function(
	contract: Contract,
	contractParameters: ContractParameters,
	contractFunding: ContractFunding,
	redeemSignature: SignatureTemplate | string,
	redeemPublicKey: string,
	oracleMessage: string,
	oracleSignature: string,
	hedgePayoutSats: number,
	longPayoutSats: number,
): Promise<Transaction>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildPayoutTransaction() <=', arguments ]);

	// Convert the contract funding to a CashScript UTXO to be used as an input
	const input = contractFundingToCoin(contractFunding);

	// Derive the hedge and long addresses from the contract parameters.
	const hedgeAddress = lockScriptToAddress(contractParameters.hedgeLockScript.slice(2));
	const longAddress = lockScriptToAddress(contractParameters.longLockScript.slice(2));

	// Parse the passed oracle price message and extract the block height
	const { blockHeight } = await OracleData.parsePriceMessage(hexToBin(oracleMessage));

	// Build the payout transaction and set its nLocktime to the block height of the
	// oracle message.
	const payoutTransaction = contract.functions
		.payout(
			// // FIXED // //
			// Outputs
			contractParameters.hedgeLockScript,
			contractParameters.longLockScript,
			// Oracle
			contractParameters.oraclePubk,
			// // REDEMPTION // //
			// Transaction verification
			redeemSignature,
			redeemPublicKey,
			// Oracle data
			oracleMessage,
			oracleSignature,
		)
		.from(input)
		.withoutChange()
		.to(hedgeAddress, hedgePayoutSats)
		.to(longAddress, longPayoutSats)
		.withTime(blockHeight);

	// Output function result for easier collection of test data.
	debug.result([ 'buildPayoutTransaction() =>', payoutTransaction ]);

	return payoutTransaction;
};

/**
 * Estimate the required miner fee to execute a redemption transaction for a contract.
 *
 * @param contract             CashScript contract instance to use in the estimation.
 * @param contractParameters   contract parameters to use in the estimation.
 * @param feeRate              amount of satoshis to pay per byte of transaction data.
 *
 * @returns an estimate of the payout transaction size for this contract.
 */
export const estimatePayoutTransactionFee = async function(
	contract: Contract,
	contractParameters: ContractParameters,
	feeRate: number = 1.0,
): Promise<number>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'estimatePayoutTransactionFee() <=', arguments ]);

	// Create placeholder data for the transaction signature / public key.
	const placeholderSignature = '00'.repeat(65);
	const placeholderPublicKey = '00'.repeat(33);

	// Create placeholder oracle data.
	const placeholderOracleMessage = '00'.repeat(50);
	const placeholderOracleSignature = '00'.repeat(64);

	// Create a placeholder 10 BCH funding (input/output values do not matter for the size calculation).
	const placeholderFunding =
	{
		fundingOutput: 0,
		fundingTransaction: '00'.repeat(32),
		fundingSatoshis: 10e8,
	};

	// Send 4 BCH to both hedge and long (input/output values do not matter for the size calculation).
	const placeholderHedgePayoutSats = 4e8;
	const placeholderLongPayoutSats = 4e8;

	// Build the placeholder payout transaction. Note that the locktime for this
	// transaction is set to 0 because of the placeholder oracle message.
	const payoutTransaction = await buildPayoutTransaction(
		contract,
		contractParameters,
		placeholderFunding,
		placeholderSignature,
		placeholderPublicKey,
		placeholderOracleMessage,
		placeholderOracleSignature,
		placeholderHedgePayoutSats,
		placeholderLongPayoutSats,
	);

	// Build the payout transaction as a raw hex string.
	const rawPayoutTransaction = await payoutTransaction.build();

	// The built transaction is a hex string, so we divide its length by 2
	const transactionSize = rawPayoutTransaction.length / 2;

	// Calculate the transaction fee based on size and fee rate.
	const transactionFee = transactionSize * feeRate;

	// Output function result for easier collection of test data.
	debug.result([ 'estimatePayoutTransactionFee() =>', transactionFee ]);

	return transactionSize;
};

/**
 * Extract the constructor parameters from a redeem script. Note that not all parameters can be
 * parsed, since some of them are hidden behind mutualRedemptionDataHash or payoutDataHash.
 *
 * @param redeemScript   redeem script extracted from a settlement transaction.
 *
 * @throws {Error} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns partial ContractParameters object, excluding the parameters hidden behind data hashes.
 */
const extractConstructorParameters = function(redeemScript: Uint8Array): ParsedParametersCommon
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'extractConstructorParameters() <=', arguments ]);

	// Parse redeem script and extract constructor parameters.
	const constructorParameters = parseBytecode(redeemScript).slice(0, 10) as AuthenticationInstructionPush[];
	const [
		maturityHeight,
		earliestLiquidationHeight,
		highLiquidationPrice,
		lowLiquidationPrice,
		lowTruncatedZeroes,
		highLowDeltaTruncatedZeroes,
		payoutSatsLowTrunc,
		hedgeUnitsXSatsPerBchHighTrunc,
		payoutDataHash,
		mutualRedemptionDataHash,
	] = constructorParameters;

	// Convert available data into a partial ContractParameters object.
	// The following parameters are unavailable because they are hidden behind data hashes:
	// oraclePubk, hedgeLockScript, longLockScript, hedgeMutualRedeemPubk, longMutualRedeemPubk.
	const contractParameters =
		{
			lowLiquidationPrice: Data.decodeInt(lowLiquidationPrice.data),
			highLiquidationPrice: Data.decodeInt(highLiquidationPrice.data),
			earliestLiquidationHeight: Data.decodeInt(earliestLiquidationHeight.data),
			maturityHeight: Data.decodeInt(maturityHeight.data),
			lowTruncatedZeroes: binToHex(lowTruncatedZeroes.data),
			highLowDeltaTruncatedZeroes: binToHex(highLowDeltaTruncatedZeroes.data),
			hedgeUnitsXSatsPerBchHighTrunc: Data.decodeInt(hedgeUnitsXSatsPerBchHighTrunc.data),
			payoutSatsLowTrunc: Data.decodeInt(payoutSatsLowTrunc.data),
		};

	// Output function result for easier collection of test data.
	debug.result([ 'extractConstructorParameters() =>', contractParameters ]);

	return contractParameters;
};

/**
 * Checks whether a passed redeem script hex string belongs to an AnyHedge contract.
 *
 * @param redeemScriptHex   redeem script extracted from a settlement transaction.
 *
 * @returns true if it matches AnyHedge bytecode, false if not.
 */
const isAnyHedgeRedeemScript = function(redeemScriptHex: string): boolean
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'isAnyHedgeRedeemScript() <=', arguments ]);

	// Retrieve artifact object. Note that this is hardcoded for AnyHedge v0.10.
	const artifact = AnyHedgeArtifacts['AnyHedge v0.10'];

	// Convert artifact's ASM to hex bytecode.
	const contractBytecode = binToHex(Data.asmToBytecode(artifact.bytecode));

	// Check that the passed redeem script includes AnyHedge bytecode.
	const redeemScriptIncludesAnyHedgeBytecode = redeemScriptHex.includes(contractBytecode);

	// Output function result for easier collection of test data.
	debug.result([ 'isAnyHedgeRedeemScript() =>', redeemScriptIncludesAnyHedgeBytecode ]);

	return redeemScriptIncludesAnyHedgeBytecode;
};

/**
 * Given a list of transaction outputs, and a hedge and long input script, return
 * the payout satoshis for the hedge and long parties.
 *
 * @param outputs              list of transaction outputs to parse
 * @param hedgeLockScriptHex   hedge lock script
 * @param longLockScriptHex    long lock script
 *
 * @returns object containing hedge and long satoshis if they could be parsed from the outputs.
 */
const parseTransactionOutputs = function(
	outputs: Output[],
	hedgeLockScriptHex?: string,
	longLockScriptHex?: string,
): { hedgeSatoshis?: number; longSatoshis?: number }
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseTransactionOutputs() <=', arguments ]);

	// Declare variables for hedge and long satoshis.
	let hedgeSatoshis;
	let longSatoshis;

	// Loop over all outputs.
	for(const output of outputs)
	{
		// Convert the output's satoshis to a number.
		const outputSatoshis = Number(binToBigIntUint64LE(output.satoshis));

		// Assign the output's satoshis to hedge satoshis if the lock script matches.
		if(binToHex(output.lockingBytecode) === hedgeLockScriptHex)
		{
			hedgeSatoshis = outputSatoshis;
		}

		// Assign the output's satoshis to long satoshis if the lock script matches.
		if(binToHex(output.lockingBytecode) === longLockScriptHex)
		{
			longSatoshis = outputSatoshis;
		}
	}

	// Assemble the extracted satoshis.
	const extractedSatoshis = { hedgeSatoshis, longSatoshis };

	// Output function result for easier collection of test data.
	debug.result([ 'parseTransactionOutputs() =>', extractedSatoshis ]);

	return extractedSatoshis;
};

/**
 * Extract the input parameters for a mutual redemption transaction and build partial
 * ContractParameters and ContractSettlement objects.
 *
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns partial ContractParameters and ContractSettlement objects.
 */
const parseMutualRedemptionTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstructionPush[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseMutualRedemptionTransaction() <=', arguments ]);

	// Extract mutual redemption input parameters.
	const [
		longMutualRedeemSig,
		hedgeMutualRedeemSig,
		longMutualRedeemPubk,
		hedgeMutualRedeemPubk,
		functionSelector,
		redeemScript,
	] = inputParameters;

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(binToHex(redeemScript.data)))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const commonParameters = extractConstructorParameters(redeemScript.data);

	// Add the mutualRedemptionDataHash variables to the parameters object.
	const parameters =
	{
		...commonParameters,
		hedgeMutualRedeemPubk: binToHex(hedgeMutualRedeemPubk.data),
		longMutualRedeemPubk: binToHex(longMutualRedeemPubk.data),
	};

	// Parse the transaction outputs to determine the hedge and long payout satoshis.
	const hedgeLockScriptHex = await buildLockScriptP2PKH(parameters.hedgeMutualRedeemPubk);
	const longLockScriptHex = await buildLockScriptP2PKH(parameters.longMutualRedeemPubk);
	const { hedgeSatoshis, longSatoshis } = parseTransactionOutputs(outputs, hedgeLockScriptHex.substring(2), longLockScriptHex.substring(2));

	// Define settlement data.
	const settlement =
	{
		spendingTransaction: transactionHashHex,
		settlementType: SettlementType.MUTUAL,
		hedgeSatoshis,
		longSatoshis,
	};

	// Derive the contract's address.
	const contractLockScript = await buildLockScriptP2SH(binToHex(redeemScript.data));
	const address = lockScriptToAddress(contractLockScript.substring(2));

	// Assemble the extracted data.
	const extractedData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parseMutualRedemptionTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Extract the input parameters for a payout transaction and build partial
 * ContractParameters and ContractSettlement objects.
 *
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns partial ContractParameters and ContractSettlement objects.
 */
const parsePayoutTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstructionPush[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parsePayoutTransaction() <=', arguments ]);

	// Extract payout input parameters.
	const [
		oracleSig,
		oracleMsg,
		preimagePubk,
		preimageSig,
		oraclePubk,
		longLockScript,
		hedgeLockScript,
		preimage,
		functionSelector,
		redeemScript,
	] = inputParameters;

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(binToHex(redeemScript.data)))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const commonParameters = extractConstructorParameters(redeemScript.data);

	// Add the payoutDataHash variables to the parameters object.
	const parameters =
	{
		...commonParameters,
		oraclePubk: binToHex(oraclePubk.data),
		hedgeLockScript: binToHex(hedgeLockScript.data),
		longLockScript: binToHex(longLockScript.data),
	};

	// Parse the oracle message.
	const parsedOracleMessage = await OracleData.parsePriceMessage(oracleMsg.data);

	// Determine whether this is a maturation or a liquidation.
	const settlementType = parsedOracleMessage.blockHeight === parameters.maturityHeight ? SettlementType.MATURATION : SettlementType.LIQUIDATION;

	// Parse the transaction outputs to determine the hedge and long payout satoshis.
	const hedgeLockScriptHex = parameters.hedgeLockScript?.substring(2);
	const longLockScriptHex = parameters.longLockScript?.substring(2);
	const { hedgeSatoshis, longSatoshis } = parseTransactionOutputs(outputs, hedgeLockScriptHex, longLockScriptHex);

	// Assemble the settlement data.
	const settlement =
	{
		spendingTransaction: transactionHashHex,
		settlementType,
		hedgeSatoshis,
		longSatoshis,
		oracleMessage: binToHex(oracleMsg.data),
		oraclePublicKey: binToHex(oraclePubk.data),
		oracleSignature: binToHex(oracleSig.data),
		oraclePrice: parsedOracleMessage.price,
	};

	// Derive the contract's address.
	const contractLockScript = await buildLockScriptP2SH(binToHex(redeemScript.data));
	const address = lockScriptToAddress(contractLockScript.substring(2));

	// Assemble the extracted data
	const extractedData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parsePayoutTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Parse a settlement transaction to extract as much data as possible, ending up with partial
 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
 * could be retrieved.
 *
 * @param settlementTransactionHex   hex string for the settlement transaction
 *
 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
 * @throws {SettlementParseError} if the transaction does not have exactly one input.
 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns partial ContractParameters, ContractSettlement, and ContractFunding objects.
 */
export const parseSettlementTransaction = async function(settlementTransactionHex: string): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseSettlementTransaction() <=', arguments ]);

	// Decode the passed transaction hex and extract the inputs and outputs
	const { inputs, outputs } = decodeTransactionUnsafe(hexToBin(settlementTransactionHex));

	// Settlement transactions can only have a single input and two outputs.
	// Note that mutual redemptions could have any number of inputs/outputs, but the most
	// common mutual redemptions (refund & early maturation) have 1/2, like payouts.
	// So arbitrary payout transactions cannot be parsed using this function.
	if(inputs.length !== 1)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly one input'));
	}
	if(outputs.length !== 2)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly two outputs'));
	}

	// Extract the only input.
	const [ input ] = inputs;

	// Assemble the funding details.
	const funding =
	{
		fundingTransaction: binToHex(input.outpointTransactionHash),
		fundingOutput: input.outpointIndex,
	};

	// Extract the only input's unlocking bytecode.
	const { unlockingBytecode } = input;

	// Parse the input bytecode, knowing that unlocking bytecode can only contain data pushes.
	const inputParameters = parseBytecode(unlockingBytecode) as AuthenticationInstructionPush[];

	// Compute the transaction's transaction hash.
	const transactionHashHex = binToHex(getTransactionHashLE(await instantiateSha256(), hexToBin(settlementTransactionHex)));

	let parsedSettlementData;

	// Mutual redemptions have 6 input parameters.
	if(inputParameters.length === 6)
	{
		parsedSettlementData = await parseMutualRedemptionTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	// Payout transactions have 10 input parameters.
	else if(inputParameters.length === 10)
	{
		parsedSettlementData = await parsePayoutTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	// Any other number of input parameters means that this is not an AnyHedge transaction.
	else
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly 6 or 10 input parameters'));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'parseSettlementTransaction() =>', parsedSettlementData ]);

	return parsedSettlementData;
};

/**
 * @deprecated This functions is deprecated. Instead you should use the parseSettlementTransaction()
 * which returns more data from a settlement transaction.
 *
 * Extract the price oracle message that was used to pay out a contract in a specific settlement transaction.
 *
 * @param settlementTransactionHex   transaction hex string of the settlement transaction.
 *
 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
 * @throws {SettlementParseError} if the transaction does not have exactly one input.
 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @throws {SettlementParseError} if no oracle message could be extracted from the settlement transaction.
 * @returns the parsed oracle price message.
 */
export const extractPriceMessageFromSettlement = async function(settlementTransactionHex: string): Promise<OraclePriceMessage>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'extractPriceMessageFromSettlement() <=', arguments ]);

	// Parse settlement data from the settlement transaction.
	const parsedSettlementData = await parseSettlementTransaction(settlementTransactionHex);

	// Extract the oracle message from the settlement data.
	const encodedMessage = parsedSettlementData?.settlement?.oracleMessage;

	// If the message could not be extracted, throw a generic error.
	if(!encodedMessage)
	{
		throw(new SettlementParseError('Could not extract oracle message from settlement transaction'));
	}

	// Parse the encoded message.
	const parsedMessage = await OracleData.parsePriceMessage(hexToBin(encodedMessage));

	// Output function result for easier collection of test data.
	debug.result([ 'extractPriceMessageFromSettlement() =>', parsedMessage ]);

	return parsedMessage;
};
