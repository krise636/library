import { binToHex, hexToBin } from '@bitauth/libauth';
import { debug as debugLogger } from 'debug';

// Initialize support for debug message management.
export const debug =
{
	action:	debugLogger('anyhedge:action'),
	object:	debugLogger('anyhedge:object'),
	errors:	debugLogger('anyhedge:errors'),
	params:	debugLogger('anyhedge:params'),
	result:	debugLogger('anyhedge:result'),
};

/**
* Utility function that creates a range of numbers
*
* @param stop   The first number outside of the range
*
* @returns an array with integers from 0 to `stop` (excluding `stop`)
*/
export const range = (stop: number): number[] => [ ...Array(stop).keys() ];

/**
 * Utility function that can be used to restore Buffers from JSON as Uint8Arrays.
 *
 * @param k   key part of a key-value pair.
 * @param v   value part of a key-value pair.
 *
 * @returns a Uint8Array restored from the `v.data` field,
 */
export const reviveBufferAsUint8Array = function(k: any, v: any): Uint8Array | any
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'reviveBufferAsUint8Array() <=', arguments ]);

	// Check if the data might be a buffer..
	const isBuffer =
	(
		v !== null
		&& typeof v === 'object'
		&& 'type' in v
		&& v.type === 'Buffer'
		&& 'data' in v
		&& Array.isArray(v.data)
	);

	// Determine what to return based on condition.
	const result = (isBuffer ? Uint8Array.from(v.data) : v);

	// Output function result for easier collection of test data.
	debug.result([ 'reviveBufferAsUint8Array() =>', result ]);

	// Return the parsed content.
	return result;
};

/**
* Utility function that checks whether a passed JS number is an integer
*
* @param num   The number to check
*
* @returns a boolean indication the integer-ness of the number
*/
export const isInt = (num: number): boolean => (num % 1 === 0);

/**
 * Apply binToHex to all properties of an object.
 *
 * @param object   object with only Uint8Arrays as property values.
 *
 * @returns an object with all property values being hex strings
 */
export const propertiesBinToHex = function(
	object: { [key: string]: Uint8Array },
): { [key: string]: string }
{
	// Loop over all key-value pairs in the object, apply binToHex to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: binToHex(value) }), {});
};

/**
 * Apply hexToBin to all properties of an object
 *
 * @param object   object with only hex strings as property values
 *
 * @returns an object with all property values being Uint8Arrays
 */
export const propertiesHexToBin = function(
	object: { [key: string]: string },
): { [key: string]: Uint8Array }
{
	// Loop over all key-value pairs in the object, apply hexToBin to the values,
	// and add the new key-value pair to a result object.
	return Object.entries(object).reduce((accumulator, [ key, value ]) => ({ ...accumulator, [key]: hexToBin(value) }), {});
};
