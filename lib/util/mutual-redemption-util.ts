import { AuthenticationProgramStateBCH, AuthenticationTemplate, authenticationTemplateToCompilerBCH, bigIntToBinUint64LE, BytecodeGenerationErrorBase, CompilationData, CompilationEnvironmentBCH, Compiler, extractResolvedVariableBytecodeMap, generateTransaction, hexToBin, InputTemplate, Output, Transaction, TransactionContextBCH, TransactionGenerationAttempt, TransactionGenerationError } from '@bitauth/libauth';
import { Utxo } from 'cashscript';
import { ContractParameters, RedemptionDataBin, RedemptionDataHex, SignedTransactionProposal, UnsignedTransactionProposal } from '../interfaces';
import { addressToLockScript, decodeWIF, derivePublicKey } from './bitcoincash-util';
import { propertiesHexToBin, debug, propertiesBinToHex } from './javascript-util';

type LibauthCompilerBCH = Compiler<TransactionContextBCH, CompilationEnvironmentBCH, AuthenticationProgramStateBCH>;

interface StructuredResolvedVariables
{
	locking: RedemptionDataBin[];
	unlocking: RedemptionDataBin[];
}

/**
 * Generates a libauth AuthenticationTemplate used in the mutual redemption of an AnyHedge contract.
 *
 * @param redeemScriptHex   hex string containing the redeem script for the AnyHedge contract.
 *
 * @returns the generated AuthenticationTemplate
 */
const generateMutualRedemptionTemplate = async function(
	redeemScriptHex: string,
): Promise<AuthenticationTemplate>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'generateMutualRedemptionTemplate() <=', arguments ]);

	// Define an Authentication Template for AnyHedge mutual redemption transactions.
	const template: AuthenticationTemplate =
	{
		// Version is required to be 0 by Libauth
		version: 0,
		// Define a hedge and long entity with private keys.
		entities: {
			hedge: { variables: { hedge_key: { type: 'Key' } } },
			long: { variables: { long_key: { type: 'Key' } } },
		},
		scripts: {
			// Define the lock script as a P2SH script with the provided redeem script.
			lock: { lockingType: 'p2sh', script: `0x${redeemScriptHex}` },
			// Define the unlock script based on the parameters to the mutualRedeem() contract function.
			unlock: {
				unlocks: 'lock',
				script: '<long_key.schnorr_signature.all_outputs> <hedge_key.schnorr_signature.all_outputs> <long_key.public_key> <hedge_key.public_key> OP_0',
			},
		},
		supported: [ 'BCH_2020_05' ],
	};

	// Output function result for easier collection of test data.
	debug.result([ 'generateMutualRedemptionTemplate() =>', template ]);

	return template;
};

/**
 * Map a CashScript UTXO to a libauth InputTemplate used with a libauth compiler
 * in libauth's transaction generation attempts.
 *
 * @param coin            CashScript UTXO to be mapped
 * @param compiler        Libauth Compiler to use for the transaction generations
 * @param unlockingData   Libauth unlocking data used to unlock the input
 *
 * @returns mapped input template
 */
const contractCoinToInputTemplate = function(
	coin: Utxo,
	compiler: LibauthCompilerBCH,
	unlockingData: CompilationData<never>,
): InputTemplate<LibauthCompilerBCH>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractCoinToInputTemplate() <=', arguments ]);

	// Map the CashScript UTXO's properties to InputTemplate properties.
	const inputTemplate =
	{
		outpointIndex: coin.vout,
		outpointTransactionHash: hexToBin(coin.txid),
		// Default nSequence number of 0 (no relative locktime)
		sequenceNumber: 0,
		unlockingBytecode:
		{
			compiler,
			satoshis: bigIntToBinUint64LE(BigInt(coin.satoshis)),
			script: 'unlock',
			data: unlockingData,
		},
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractCoinToInputTemplate() =>', inputTemplate ]);

	return inputTemplate;
};

/**
 * Generate unlocking data from a private key WIF.
 *
 * @param privateKeyWIF        private key WIF of the current signing party
 * @param contractParameters   contract parameters of the relevant contract
 *
 * @throws {Error} if the private key WIF string is not valid.
 * @throws {Error} if the private key WIF does not belong to either party of the contract.
 * @returns An unlocking data object for the provided WIF
 */
export const unlockingDataFromWIF = async function(
	privateKeyWIF: string,
	contractParameters: ContractParameters,
): Promise<CompilationData<never>>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'unlockingDataFromWIF() <=', arguments ]);

	// Decode the private key WIF.
	const privateKey = await decodeWIF(privateKeyWIF);

	// Derive the corresponding public key from the private key.
	const derivedPublicKey = await derivePublicKey(privateKey);

	// Derive the correct key ID for the signing party
	// or throw an error if the derived public key does not belong to either party.
	// Note: it is not possible to create a contract with the same public key for both parties.
	let keyID: string;
	if(derivedPublicKey === contractParameters.hedgeMutualRedeemPubk)
	{
		keyID = 'hedge_key';
	}
	else if(derivedPublicKey === contractParameters.longMutualRedeemPubk)
	{
		keyID = 'long_key';
	}
	else
	{
		throw(new Error('The passed private key WIF does not belong to either party in the contract.'));
	}

	// Create the unlocking data in the format that libauth expects. This variant of
	// unlocking data includes private keys, which will be resolved in transaction generation.
	const unresolvedUnlockingData =
	{
		keys: { privateKeys: { [keyID]: hexToBin(privateKey) } },
	};

	// Output function result for easier collection of test data.
	debug.result([ 'unlockingDataFromWIF() =>', unresolvedUnlockingData ]);

	return unresolvedUnlockingData;
};

/**
 * Generate libauth unlocking data from redemption data.
 *
 * @param redemptionData redemption data to convert to unlocking data.
 *
 * @returns unlocking data corresponding to the passed redemption data.
 */
export const unlockingDataFromRedemptionData = function(
	redemptionData: RedemptionDataHex,
): CompilationData<never>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'unlockingDataFromRedemptionData() <=', arguments ]);

	// Convert the RedemptionData to the correct format expected by libauth. This variant of
	// unlocking data includes the already-resolved data.
	const resolvedUnlockingData = { bytecode: propertiesHexToBin(redemptionData) };

	// Output function result for easier collection of test data.
	debug.result([ 'unlockingDataFromRedemptionData() =>', resolvedUnlockingData ]);

	return resolvedUnlockingData;
};

/**
 * Convert a transaction proposal into a libauth transaction details object.
 *
 * @param transactionProposal   transaction proposal to be converted.
 *
 * @returns a corresponding transaction details object.
 */
// Note: This uses CashScript UTXOs in place of libauth inputs because these retain
//       information about the amounts. These are replaced during transaction generation.
export const proposalToTransactionDetails = function(
	transactionProposal: UnsignedTransactionProposal,
): Transaction<Utxo, Output>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'proposalToTransactionDetails() <=', arguments ]);

	// Extract inputs and locktime from the transaction proposal.
	const { inputs, locktime = 0 } = transactionProposal;

	// Extract outputs from the transaction proposal and convert them to the expected format.
	const outputs = transactionProposal.outputs.map((output) =>
	{
		const lockingBytecode = hexToBin(addressToLockScript(output.to));
		const satoshis = bigIntToBinUint64LE(BigInt(output.amount));

		return { lockingBytecode, satoshis };
	});

	// Assemble transaction details.
	const transactionDetails = { inputs, outputs, locktime, version: 2 };

	// Output function result for easier collection of test data.
	debug.result([ 'proposalToTransactionDetails() =>', transactionDetails ]);

	return transactionDetails;
};

/**
 * Extract redemption data list from a TransactionGenerationError object.
 *
 * Note: this function was implemented by Jason Dreyzehner (author of Libauth),
 * and changed to fit our linting rules by me. This will likely be integrated into
 * the libauth library in some different form, after which we can remove it from here.
 *
 * @param transactionGenerationError   failed transaction generation attempt.
 *
 * @returns a structured object that contains *all* resolved variables in the failed transaction generation attempt.
 */
export const extractRedemptionDataList = function(
	transactionGenerationError: TransactionGenerationError,
): RedemptionDataHex[]
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'extractRedemptionDataList() <=', arguments ]);

	// Extract errors list from the transaction generation attempt.
	const errors = transactionGenerationError.errors as BytecodeGenerationErrorBase[];

	const structuredResolvedVariables = errors.reduce<StructuredResolvedVariables>(
		(accumulator, error) =>
		{
			// Retrieve already stored resolutions for the current type and input
			const siblingResolutions = accumulator[error.type][error.index] ?? {};

			// Copy this into a new variable
			let newResolutions = siblingResolutions;

			// Add new resolved variables to these resolutions
			if(error.resolved !== undefined)
			{
				const extractedResolutions = extractResolvedVariableBytecodeMap(error.resolved);
				newResolutions = { ...siblingResolutions, ...extractedResolutions };
			}

			// Store them in the accumulator.
			accumulator[error.type].splice(error.index, 0, newResolutions);

			return accumulator;
		},
		{ locking: [], unlocking: [] },
	);

	// Extract the correct variables and convert the values to hex strings.
	const redemptionDataList = structuredResolvedVariables.unlocking.map(propertiesBinToHex);

	// Output function result for easier collection of test data.
	debug.result([ 'extractRedemptionDataList() =>', redemptionDataList ]);

	return redemptionDataList;
};

/**
 * Attempt to generate a transaction using a transaction proposal and relevant parameters.
 *
 * @param transactionProposal     transaction proposal to attempt generation with.
 * @param redeemScriptHex         redeem script hex string of the relevant contract.
 * @param unlockingDataPerInput   unlocking data for each of the transaction proposal's inputs.
 *
 * @returns a successful or failed transaction generation attempt.
 */
export const attemptTransactionGeneration = async function(
	transactionProposal: UnsignedTransactionProposal,
	redeemScriptHex: string,
	unlockingDataPerInput: CompilationData<never>[],
): Promise<TransactionGenerationAttempt>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'attemptTransactionGeneration() <=', arguments ]);

	// Generate a libauth AuthenticationTemplate for mutual redemption.
	const template = await generateMutualRedemptionTemplate(redeemScriptHex);

	// Generate a libauth compiler from the AuthenticationTemplate.
	const compiler = await authenticationTemplateToCompilerBCH(template);

	// Convert all of the proposal's inputs to libauth input templates
	const inputs = transactionProposal.inputs.map((coin, i) => contractCoinToInputTemplate(coin, compiler, unlockingDataPerInput[i]));

	// Convert the proposal into a transactionDetails object.
	const transactionDetails = proposalToTransactionDetails(transactionProposal);

	// Attempt to generate a transaction using the transaction details and input templates
	const transactionGenerationAttempt = generateTransaction({ ...transactionDetails, inputs });

	// Output function result for easier collection of test data.
	debug.result([ 'attemptTransactionGeneration() =>', transactionGenerationAttempt ]);

	return transactionGenerationAttempt;
};

/**
 * Merge the redemption data of two separate transaction proposals.
 *
 * @param signedProposal1   first signed transaction proposal.
 * @param signedProposal2   second signed transaction proposal.
 *
 * @throws {Error} if the transaction details of the proposals don't match.
 * @throws {Error} if the redemption data lists of the proposals have different lengths.
 * @returns a new signed proposal with merged redemption data.
 */
export const mergeSignedProposals = function(
	signedProposal1: SignedTransactionProposal,
	signedProposal2: SignedTransactionProposal,
): SignedTransactionProposal
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'mergeSignedProposals() <=', arguments ]);

	// Check that the transaction details of both proposals match.
	const transactionDetails1 = proposalToTransactionDetails(signedProposal1);
	const transactionDetails2 = proposalToTransactionDetails(signedProposal2);
	if(JSON.stringify(transactionDetails1) !== JSON.stringify(transactionDetails2))
	{
		throw(new Error('The passed proposals cannot be merged because they have different transaction details.'));
	}

	// Check that the redemption data lists of both proposals are the same.
	if(signedProposal1.redemptionDataList.length !== signedProposal2.redemptionDataList.length)
	{
		throw(new Error('The passed proposals cannot be merged because their redemption data lists have different lengths.'));
	}

	// Merge the redemption data of both proposals
	const mergedRedemptionData = signedProposal1.redemptionDataList.map((data1, i) =>
	{
		// Retrieve corresponding data entry from the second list
		const data2 = signedProposal2.redemptionDataList[i];

		// Merge the data
		const mergedData = { ...data1, ...data2 };

		return mergedData;
	});

	const mergedProposal = { ...signedProposal1, redemptionDataList: mergedRedemptionData };

	// Output function result for easier collection of test data.
	debug.result([ 'mergeSignedProposals() =>', mergedProposal ]);

	return mergedProposal;
};
