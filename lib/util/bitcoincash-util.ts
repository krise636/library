import { hexToBin, cashAddressToLockingBytecode, instantiateSha256, instantiateRipemd160, HashFunction, flattenBinArray, decodePrivateKeyWif, instantiateSecp256k1, encodeCashAddress, CashAddressType, binToHex, isHex, bigIntToScriptNumber, lockingBytecodeToCashAddress } from '@bitauth/libauth';
import { ElectrumNetworkProvider, NetworkProvider } from 'cashscript';
import { IncorrectWIFError } from '../errors';
import { debug } from './javascript-util';

// Wrapper around libauth's hash functions
const hash = function(payload: Uint8Array, hashFunction: HashFunction): Uint8Array
{
	return hashFunction.hash(payload);
};

/**
 * Sha256 hash a payload
 *
 * @param payload   Payload to be hashed
 *
 * @returns the sha256 hash of the payload
 */
export const sha256 = async function(payload: Uint8Array): Promise<Uint8Array>
{
	return hash(payload, await instantiateSha256());
};

/**
 * Ripemd160 hash a payload
 *
 * @param payload   Payload to be hashed
 *
 * @returns the ripemd160 hash of the payload
 */
export const ripemd160 = async function(payload: Uint8Array): Promise<Uint8Array>
{
	return hash(payload, await instantiateRipemd160());
};

/**
 * Sha256 hash a payload, then ripemd160 hash the result
 *
 * @param payload   Payload to be hashed
 *
 * @returns the ripemd160 hash of the sha256 hash of the payload
 */
export const hash160 = async function(payload: Uint8Array): Promise<Uint8Array>
{
	return ripemd160(await sha256(payload));
};

/**
 * Decode a private key into a hex-encoded private key.
 *
 * @param privateKeyWIF   private key WIF string to be decoded.
 *
 * @throws {Error} if the WIF string has an incorrect format.
 * @returns The decoded hex encoded private key.
 */
export const decodeWIF = async function(privateKeyWIF: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'decodeWIF() <=', arguments ]);

	// Attempt to decode the private key WIF.
	const decodeResult = decodePrivateKeyWif(await instantiateSha256(), privateKeyWIF);

	// If the decode result is a string, it failed, so we throw an error.
	if(typeof decodeResult === 'string')
	{
		throw(new IncorrectWIFError(privateKeyWIF));
	}

	// Extract the result and convert it to hex.
	const privateKey = binToHex(decodeResult.privateKey);

	// Output function result for easier collection of test data.
	debug.result([ 'decodeWIF() =>', privateKey ]);

	return privateKey;
};

/**
 * Derive the compressed public key hex string corresponding to a private key (WIF or hex string)
 *
 * @param privateKeyOrWIF a BCH private key (WIF or hex string)
 *
 * @throws {Error} if the passed WIF string is invalid
 * @returns the compressed public key corresponding to the passed private key (WIF or hex string)
 */
export const derivePublicKey = async function(privateKeyOrWIF: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'derivePublicKey() <=', arguments ]);

	// If the passed parameter is a hex string, it is assumed to privateKey be a private,
	// otherwise it's assumed to be a WIF, and gets decoded.
	const privateKey = (isHex(privateKeyOrWIF) ? privateKeyOrWIF : await decodeWIF(privateKeyOrWIF));

	// Derive the public key from the private key
	const secp256k1 = await instantiateSecp256k1();
	const publicKey = binToHex(secp256k1.derivePublicKeyCompressed(hexToBin(privateKey)));

	// Output function result for easier collection of test data.
	debug.result([ 'derivePublicKey() =>', publicKey ]);

	return publicKey;
};

/**
 * Encode a compressed public key into a P2PKH cashaddress
 *
 * @param publicKeyHex   Compressed public key to encode into a P2PKH cashaddress
 *
 * @returns a P2PKH cashaddress corresponding to the passed compressed public key
 */
export const encodeCashAddressP2PKH = async function(publicKeyHex: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'encodeCashAddressP2PKH() <=', arguments ]);

	const publicKeyHash = await hash160(hexToBin(publicKeyHex));
	const cashAddress = encodeCashAddress('bitcoincash', CashAddressType.P2PKH, publicKeyHash);

	// Output function result for easier collection of test data.
	debug.result([ 'encodeCashAddressP2PKH() =>', cashAddress ]);

	return cashAddress;
};

/**
 * Helper function to construct a P2PKH locking script hex string from a compressed public key hex string
 *
 * @param publicKeyHex   Compressed public key hex string to create a P2PKH locking script for
 *
 * @returns a P2PKH locking script hex string corresponding to the passed compressed public key hex string
 */
export const buildLockScriptP2PKH = async function(publicKeyHex: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildLockScriptP2PKH() <=', arguments ]);

	const publicKeyHash = await hash160(hexToBin(publicKeyHex));

	// Build P2PKH lock script (PUSH<25> DUP HASH160 PUSH<20> <public key hash> EQUALVERIFY CHECKSIG)
	const lockScript = binToHex(flattenBinArray([ hexToBin('1976a914'), publicKeyHash, hexToBin('88ac') ]));

	// Output function result for easier collection of test data.
	debug.result([ 'buildLockScriptP2PKH() =>', lockScript ]);

	return lockScript;
};

/**
 * Helper function to construct a P2SH locking script hex string from a script bytecode hex string
 *
 * @param scriptBytecodeHex   Bytecode hex string of the script for which to create a P2SH locking script hex string
 *
 * @returns a P2SH locking script hex string corresponding to the passed script bytecode hex string
 */
export const buildLockScriptP2SH = async function(scriptBytecodeHex: string): Promise<string>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildLockScriptP2SH() <=', arguments ]);

	const scriptHash = await hash160(hexToBin(scriptBytecodeHex));

	// Build P2SH lock script (PUSH<23> HASH160 PUSH<20> <script hash> EQUAL)
	const lockScript = binToHex(flattenBinArray([ hexToBin('17a914'), scriptHash, hexToBin('87') ]));

	// Output function result for easier collection of test data.
	debug.result([ 'buildLockScriptP2SH() =>', lockScript ]);

	return lockScript;
};

/**
 * Helper function to convert an address to a locking script hex string
 *
 * @param address   Address to convert to locking script hex string
 *
 * @returns a locking script hex string corresponding to the passed address
 */
export const addressToLockScript = function(address: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'addressToLockScript() <=', arguments ]);

	const result = cashAddressToLockingBytecode(address);

	// The `cashAddressToLockingBytecode()` call returns an error string OR the correct bytecode
	// so we check if it errors, in which case we throw the error, otherwise return the result
	if(typeof result === 'string') throw(new Error(result));

	const lockScript = binToHex(result.bytecode);

	// Output function result for easier collection of test data.
	debug.result([ 'addressToLockScript() =>', lockScript ]);

	return lockScript;
};

/**
 * Helper function to convert a locking script hex to a cashaddress
 *
 * @param lockScript   lock script to be converted to cashaddress
 *
 * @returns a cashaddress corresponding to the passed lock script
 */
export const lockScriptToAddress = function(lockScript: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'lockScriptToAddress() <=', arguments ]);

	// Convert the lock script to a cashaddress (with bitcoincash: prefix).
	const address = lockingBytecodeToCashAddress(hexToBin(lockScript), 'bitcoincash');

	// A successful conversion will result in a string, unsuccessful will return AddressContents
	if(typeof address !== 'string')
	{
		throw(new Error(`Provided lock script ${lockScript} cannot be converted to address ${JSON.stringify(address)}`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'lockScriptToAddress() =>', address ]);

	return address;
};

/**
 * Encode a number as a hex encoded script number
 *
 * @param num   number to be encoded
 *
 * @returns hex encoded script number
 */
export const hexEncodeScriptNum = function(num: number): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'hexEncodeScriptNum() <=', arguments ]);

	const scriptNum = bigIntToScriptNumber(BigInt(num));
	const scriptNumHex = binToHex(scriptNum);

	// Output function result for easier collection of test data.
	debug.result([ 'hexEncodeScriptNum() =>', scriptNumHex ]);

	return scriptNumHex;
};

/**
 * Broadcast a raw transaction to the BCH blockchain.
 *
 * @param transactionHex      transaction hex to be broadcasted
 * @param [networkProvider]   custom network provider to use when broadcasting
 *
 * @throws {Error} if the transaction fails to broadcast
 * @returns the transaction ID
 */
export const broadcastTransaction = async function(transactionHex: string, networkProvider?: NetworkProvider): Promise<string>
{
	// Use the passed provider or instantiate a new "default" ElectrumNetworkProvider.
	const provider = networkProvider || new ElectrumNetworkProvider();

	// broadcast the raw transaction and retrieve the transaction ID.
	const transactionID = await provider.sendRawTransaction(transactionHex);

	return transactionID;
};
